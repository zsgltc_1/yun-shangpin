import request from '/@/utils/request';

 
export function getMenuAdmin(params?: object) {
	return request({
		url: '/users/menus',
		method: 'POST',
		params,
	});
}