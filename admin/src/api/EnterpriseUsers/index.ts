import request from '/@/utils/request';

 
export function usePageApis() {
	return {
		getlistAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseusers/getlist',
				method: 'post',
				params,
			});
		},
		getMenuTest: (params?: object) => {
			return request({
				url: 'https://api.pydz.net/api/menus',
				method: 'get',
				params,
			});
		},
		/**
		 *  启用账号
		 * @param params 要传的参数值
		 * @returns 返回接口数据
		 */
		enableAPI: (params: object) => {
			return request({
				url: '/users/Enterpriseusers/enable',
				method: 'post',
				params,
			});
		},
		/**
		 *  禁用账号
		 * @param params 要传的参数值
		 * @returns 返回接口数据
		 */
		disableAPI: (params: object) => {
			return request({
				url: '/users/Enterpriseusers/disable',
				method: 'post',
				params,
			});
		}
	};
}