import request from '/@/utils/request';
 
/**
 *  上传自己的头像
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function uploadFaceAPI(params: FormData) {
	return request({
		url: 'users/usercenter/uploadface',
		method: 'post',
		data: params,
	});
}
/**
 *  修改自己的密码
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function changePasswordAPI(params: object) {
	 
	return request({
		url: 'users/usercenter/changepassword',
		method: 'post',
		data: params,
	});
}

/**
 * 修改用户资料
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function editInfoApI(params: object) {
	 
	return request({
		url: 'users/usercenter/editinfo',
		method: 'post',
		data: params,
	});
}