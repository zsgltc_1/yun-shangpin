import request from '/@/utils/request';
/**
 *  菜单树数据
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function TreeAPI(params: object) {
	return request({
		url: '/users/SystemConfigMenus/tree',
		method: 'post',
		data: params,
	});
}

/**
 * 获取一个数据
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function GetModelAPI(params: object) {
	return request({
		url: '/users/SystemConfigMenus/getmodel',
		method: 'post',
		data: params,
	});
}
/**
 * 保存数据
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function SaveAPI(params: object) {
	return request({
		url: '/users/SystemConfigMenus/save',
		method: 'post',
		data: params,
	});
}
/**
 *  移动节点
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function MoveToAPI(params: object) {
	return request({
		url: '/users/SystemConfigMenus/moveto',
		method: 'post',
		data: params,
	});
}
/**
 *  删除
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function DelAPI(params: object) {
	return request({
		url: '/users/SystemConfigMenus/del',
		method: 'post',
		data: params,
	});
}
/**
 *  检查key是否占用
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function KeyExistAPI(params: object) {
	return request({
		url: '/users/SystemConfigMenus/key/exist',
		method: 'post',
		data: params,
	});
}
/**
 *  获取uri
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function GetURIAPI(params: object) {
	return request({
		url: '/users/EnterpriseMenus/geturi',
		method: 'post',
		data: params,
	});
}
/**
 *  保存uri
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function SaveURIAPI(params: object) {
	return request({
		url: '/users/EnterpriseMenus/saveuri',
		method: 'post',
		data: params,
	});
}
/**
 *  删除uri
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function DelteteURIAPI(params: object) {
	return request({
		url: '/users/EnterpriseMenus/delteteuri',
		method: 'post',
		data: params,
	});
}
