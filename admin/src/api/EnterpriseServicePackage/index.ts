import request from '/@/utils/request';

 
export function usePageApis() {
	return {
		getlistAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseservicepackage/getlist',
				method: 'post',
				params,
			});
		},
		createAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseservicepackage/create',
				method: 'post',
				params,
			});
		},
		updateAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseservicepackage/update',
				method: 'post',
				params,
			});
		},
		enableAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseservicepackage/enable',
				method: 'post',
				params,
			});
		},
		disableAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseservicepackage/disable',
				method: 'post',
				params,
			});
		},
		deleteAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseservicepackage/delete',
				method: 'post',
				params,
			});
		},
	};
}