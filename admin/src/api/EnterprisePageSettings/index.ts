import request from '/@/utils/request';

 
export function usePageApis() {
	return {
		getModelAPI: (params?: object) => {
			return request({
				url: '/users/EnterprisePageSettings/GetModel',
				method: 'post',
				params,
			});
		},
		saveModelAPI: (params?: object) => {
			return request({
				url: '/users/EnterprisePageSettings/SaveModel',
				method: 'post',
				params,
			});
		},
	};
}