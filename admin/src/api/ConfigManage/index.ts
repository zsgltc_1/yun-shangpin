import request from '/@/utils/request';
/**
 *  获取配置菜单
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function ParentListAPI(params: object) {
	return request({
		url: '/users/ConfigManage/parentlist',
		method: 'post',
		data: params,
	});
}
/**
 * 获取配置菜单数据
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function GetListAPI(params: object) {
	return request({
		url: '/users/ConfigManage/getlist',
		method: 'post',
		data: params,
	});
}
/**
 * 保存数据
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function SaveAPI(params: object) {
	return request({
		url: '/users/ConfigManage/save',
		method: 'post',
		data: params,
	});
}