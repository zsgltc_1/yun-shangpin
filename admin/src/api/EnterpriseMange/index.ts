import request from '/@/utils/request';

 
export function usePageApis() {
	return {
		getlistAPI: (params?: object) => {
			return request({
				url: '/users/Enterprise/getlist',
				method: 'post',
				params,
			});
		},
		getModel: (params?: object) => {
			return request({
				url: '/users/Enterprise/getmodel',
				method: 'post',
				params,
			});
		},
		/**
		 * 创建用户
		 * @param params 要传的参数值
		 * @returns 返回接口数据
		 */
		createAPI: (params: object) => {
			return request({
				url: '/users/Enterpriseusers/create',
				method: 'post',
				params,
			});
		},
		/**
		 *  检查账户名
		 * @param params 要传的参数值
		 * @returns 返回接口数据
		 */
		existAPI: (params: object) => {
			return request({
				url: '/users/Enterpriseusers/exist',
				method: 'post',
				params,
			});
		}
	};
}