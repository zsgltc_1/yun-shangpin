import axios from 'axios';
import { ElMessage, ElMessageBox } from 'element-plus';
import { Session } from '/@/utils/storage';

// 配置新建一个 axios 实例
const service = axios.create({
	baseURL: import.meta.env.VITE_API_URL as any,
	timeout: 50000,
	headers: { 'Content-Type': 'application/json', },
	//处理长整数精度丢失的问题
	transformResponse: [function(data) {
		try {
			data = data.replace(/"\w+"\s*:\s*\d{16,}/g, function(longVal) {
				let split = longVal.split(":");
				return split[0] + ':' + '"' + split[1].trim() + '"';
			});
	
			return JSON.parse(data);
		} catch (err) {
			return data;
		}
	
	}]
});

// 添加请求拦截器
service.interceptors.request.use(
	(config) => {
		 if (`${Session.get('token')}`) {
		 	if (config.data) {
		 		if (!(config.data instanceof FormData)) {
		 			//console.log(config.data,config.data instanceof FormData)
		 			config = Object.assign(config, { data: JSON.parse(JSON.stringify(config.data)) });
		 			config.data["token"] = `${Session.get('token')}`;
		 		}
		 		if (config.data instanceof FormData) {
		 			config.data.append("token", `${Session.get('token')}`);
		 		}
		 	} else {
		 		Object.assign(config, { data: { token: `${Session.get('token')}` } });
		 	}
		 }
		 return config;
 
	},
	(error) => {
		// 对请求错误做些什么
		return Promise.reject(error);
	}
);

// 添加响应拦截器
service.interceptors.response.use(
	(response) => {
		 const res = response.data;
		 if (res.code && res.code !== 0) {
		 	ElMessage.error(`${res.message}`);
		 	return response.data;
		 } else {
		 
		 	return response.data;
		 }
	},
	(error) => {
		 switch (JSON.parse(JSON.stringify(error)).status) {
		 	case 401:
		 		ElMessageBox.alert('你已被登出，请重新登录', '提示', {}).then(() => { })
		 			.catch(() => { });
		 		setTimeout(() => {
		 			Session.clear(); // 清除浏览器全部临时缓存
		 			window.location.reload();
		 		}, 500);
		 		break;
		 	case 403:
		 		ElMessageBox.alert('你们没有该项操作的权限！', '提示', {}).then(() => { })
		 			.catch(() => { });
		 		break;
		 }
		 
		 if (error.message.indexOf('timeout') != -1) {
		 	ElMessage.error('网络超时');
		 } else if (error.message == 'Network Error') {
		 	ElMessage.error('网络连接错误');
		 } else {
		 	if (error.response.data) ElMessage.error(error.response.statusText);
		 	else ElMessage.error('接口路径找不到');
		 }
		 return Promise.reject(error)
	}
);

// 导出 axios 实例
export default service;
