import { defineStore } from 'pinia';
import { UserInfosStates } from './interface';
import { Session } from '/@/utils/storage';

/**
 * 用户信息
 * @methods setUserInfos 设置用户信息
 */
export const useUserInfo = defineStore('userInfo', {
	state: (): UserInfosStates => ({
		userInfos: {
			ID:'',
			AccountName: '',
			Avatar: '',
			Roles: '',
			Name:'',
			Number: '',
			Password:'',
			MobileTelephone:'',
			Email:'',
			QQ:'',
			Wechat:'',
			LoginCount:0,
			LastIp:'',
			LoginTime:'0',
            roles:[]
		},
	}),
	actions: {
		async setUserInfos() {
			this.userInfos = Session.get('userInfo');
		},
 
	},
});
