import request from '/@/utils/request';

 
export function usePageApis() {
	return {
		getlistAPI: (params?: object) => {
			return request({
				url: '/users/Enterprise/getlist',
				method: 'post',
				params,
			});
		},
		getModel: (params?: object) => {
			return request({
				url: '/users/Enterprise/getmodel',
				method: 'post',
				params,
			});
		},
	};
}