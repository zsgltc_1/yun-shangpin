import request from '/@/utils/request';
/**
 * 用户列表
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function getListAPI(params: object) {
	return request({
		url: '/users/getlist',
		method: 'post',
		data: params,
	});
}
/**
 * 创建用户
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function createAPI(params: object) {
	return request({
		url: '/users/create',
		method: 'post',
		data: params,
	});
}
/**
 * 修改用户资料
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function setInfoAPI(params: object) {
	return request({
		url: '/users/setinfo',
		method: 'post',
		data: params,
	});
}
/**
 *  启用账号
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function enableAPI(params: object) {
	return request({
		url: '/users/enable',
		method: 'post',
		data: params,
	});
}
/**
 *  禁用账号
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function disableAPI(params: object) {
	return request({
		url: '/users/disable',
		method: 'post',
		data: params,
	});
}
/**
 *  检查账户名
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function existAPI(params: object) {
	return request({
		url: '/users/exist',
		method: 'post',
		data: params,
	});
}
/**
 *  上传自己的头像
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function uploadFaceAPI(params: FormData) {
	return request({
		url: '/users/uploadface',
		method: 'post',
		data: params,
	});
}

/**
 *  设置用户头像
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function uploadUsersFaceAPI(params: FormData) {
	return request({
		url: '/users/uploadusersface',
		method: 'post',
		data: params,
	});
}

/**
 *  修改自己的密码
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function changePasswordAPI(params: object) {
	return request({
		url: '/users/changepassword',
		method: 'post',
		data: params,
	});
}
/**
 *  设置密码
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function setPassAPI(params: object) {
	return request({
		url: '/users/setpass',
		method: 'post',
		data: params,
	});
}
export function refreshAPI(params: object) {
	return request({
		url: '/users/refresh',
		method: 'post',
		data: params,
	});
}
/**
 *  获取所有角色与用的关系列表
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function getUsersRolesAPI(params: object) {
	return request({
		url: '/users/getallusersrolesanduserid',
		method: 'post',
		data: params,
	});
}
/**
 *  保存用户角色数据
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function saveRolesAPI(params: object) {
	return request({
		url: '/users/saveroles',
		method: 'post',
		data: params,
	});
}

/**
 * 修改用户资料
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function editInfoApI(params: object) {
	return request({
		url: '/users/editinfo',
		method: 'post',
		data: params,
	});
}