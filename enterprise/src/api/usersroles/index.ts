import request from '/@/utils/request';
/**
 *  分页列表
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function getlistAPI(params: object) {
	return request({
		url: '/users/usersroles/getlist',
		method: 'post',
		data:   params,
	});
}
/**
 * 创建
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function createAPI(params: object) {
	return request({
		url: '/users/usersroles/create',
		method: 'post',
		data:  params,
	});
}

/**
 * 修改
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function setInfoAPI(params: object) {
	return request({
		url: '/users/usersroles/setinfo',
		method: 'post',
		data: params,
	});
}
/**
*   启用
* @param params 要传的参数值
* @returns 返回接口数据
*/
export function enableAPI(params: object) {
	return request({
		url: '/users/usersroles/enable',
		method: 'post',
		data:  params,
	});
}
/**
*   禁用
* @param params 要传的参数值
* @returns 返回接口数据
*/
export function disableAPI(params: object) {
	return request({
		url: '/users/usersroles/disable',
		method: 'post',
		data: params,
	});
}
/**
*   删除
* @param params 要传的参数值
* @returns 返回接口数据
*/
export function delAPI(params: object) {
	return request({
		url: '/users/usersroles/del',
		method: 'post',
		data:  params,
	});
}
/**
*   菜单树
* @param params 要传的参数值
* @returns 返回接口数据
*/
export function treeAPI(params: object) {
	return request({
		url: '/users/usersroles/tree',
		method: 'post',
		data:   params,
	});
}

/**
*   保存权限
* @param params 要传的参数值
* @returns 返回接口数据
*/
export function treeSaveAPI(params: object) {
	return request({
		url: '/users/usersroles/treesave',
		method: 'post',
		data: params,
	});
}
 
/**
*   获取角色成员
* @param params 要传的参数值
* @returns 返回接口数据
*/
export function getUsersMembersAPI(params: object) {
	return request({
		url: '/users/usersroles/getusersmembers',
		method: 'post',
		data: params,
	});
}
/**
*   保存角色成员
* @param params 要传的参数值
* @returns 返回接口数据
*/
export function saveMembersAPI(params: object) {
	return request({
		url: '/users/usersroles/savemembers',
		method: 'post',
		data: params,
	});
}
