import request from '/@/utils/request';

 
export function usePageApis() {
	return {
		getlistAPI: (params?: object) => {
			return request({
				url: '/users/Enterpriseservicepackage/getlist',
				method: 'post',
				params,
			});
		},
		getMenuTest: (params?: object) => {
			return request({
				url: 'https://api.pydz.net/api/menus',
				method: 'get',
				params,
			});
		},
	};
}