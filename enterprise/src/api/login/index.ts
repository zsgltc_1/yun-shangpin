import request from '/@/utils/request';

/**
 * 用户登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function signInAPI(params: object) {
	return request({
		url: '/login/enterprise',
		method: 'post',
		data: params,
	});
}


/**
 * 获取验证码
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
 export function captchasAPI() {
	return request({
		url: '/captchas',
		method: 'post',
	});
}