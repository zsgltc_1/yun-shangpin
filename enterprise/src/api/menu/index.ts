import request from '/@/utils/request';

 
export function getMenuAdmin(params?: object) {
	return request({
		url: '/enterprise/menus',
		method: 'POST',
		params,
	});
}