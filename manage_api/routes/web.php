<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\devController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/dev',[devController::class,'index']);
Route::post('/dev/createrepository', [devController::class,'createrepository']);
Route::post('/dev/createrepository2', [devController::class,'createrepository2']);
Route::get('/dev/createform/{table_name}', [devController::class,'createForm']);

Route::get('/',function(){
	 if (substr(php_uname(), 0, 7) == "Windows"){
	    
	  	 $routeCollection = \Route::getRoutes();
	  	 return count($routeCollection);
	 	}
	 $snowflake = app('Kra8\Snowflake\Snowflake');
            return $snowflake->next();
 
	});
Route::get('/login',function(){
	 return response()->json(['code' => 1, 'message' => '您的token无效！']);
	})->name('login');