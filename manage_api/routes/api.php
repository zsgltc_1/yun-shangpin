<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//region  地域
Route::any('/area', 'AreaController@Index');
Route::post('/area/getmodel', 'AreaController@GetModel');
Route::post('/area/getListbyparentid', 'AreaController@GetListByParentID');
Route::post('/area/getfullarea', 'AreaController@GetFullArea');
Route::any('/area/getlist', 'AreaController@GetList');

//endregion
//Route::any('/menus', 'IndexController@GetMenus');
Route::post('/login/{usertype}', 'Auth\IndexController@Login')->where('usertype', 'users|members|enterprise');
Route::any('/captchas', 'CaptchasController@code');
/**
* 后台用户角色
*/
Route::post('/users/menus', 'Admin\Users\IndexController@GetMenus');
Route::post('/users/check', 'Admin\Users\IndexController@Check');
Route::post('/users/refresh','Admin\Users\IndexController@Refresh');
Route::post('/users/getlist', 'Admin\Users\IndexController@GetList');
Route::post('/users/create', 'Admin\Users\IndexController@Create');
Route::post('/users/setinfo', 'Admin\Users\IndexController@SetInfo');
Route::post('/users/enable', 'Admin\Users\IndexController@Enable');
Route::post('/users/disable','Admin\Users\IndexController@Disable');
Route::post('/users/exist','Admin\Users\IndexController@Exist');

Route::post('/users/uploadusersface','Admin\Users\IndexController@UploadUsersFace');
Route::post('/users/setpass', 'Admin\Users\IndexController@SetPass' );
Route::post('/users/getallusersrolesanduserid', 'Admin\Users\IndexController@GetAllUsersRolesANDUserID');
Route::post('/users/saveroles', 'Admin\Users\IndexController@SaveRoles');

//region 个人中心
//usercenter/info/save
Route::post('/users/usercenter/uploadface','Admin\UserCenter\IndexController@UploadFace');
Route::post('/users/usercenter/changepassword', 'Admin\UserCenter\IndexController@ChangePassword' );
Route::post('/users/usercenter/editinfo','Admin\UserCenter\IndexController@EditInfo' );
//endregion
/**
* 后台用户结束
*/
//region后台菜单修改
 Route::post('/users/usersmenus/tree', 'Admin\UsersMenus\IndexController@Tree');
 Route::post('/users/usersmenus/save', 'Admin\UsersMenus\IndexController@Save');
 Route::post('/users/usersmenus/del','Admin\UsersMenus\IndexController@DelItem');
 Route::post('/users/usersmenus/moveto','Admin\UsersMenus\IndexController@MoveTo');
 Route::post('/users/usersmenus/getmodel','Admin\UsersMenus\IndexController@GetModel');
 Route::post('/users/usersmenus/langkey/exist', 'Admin\UsersMenus\IndexController@LangKeyExist');
 Route::post('/users/usersmenus/geturi', 'Admin\UsersMenus\IndexController@GetURI');
 Route::post('/users/usersmenus/saveuri','Admin\UsersMenus\IndexController@SaveURI');
 Route::post('/users/usersmenus/delteteuri', 'Admin\UsersMenus\IndexController@DelteteURI');
//endregion

//region 导航管理
 Route::post('/users/navigations/tree', 'Admin\Navigations\IndexController@Tree');
 Route::post('/users/navigations/save', 'Admin\Navigations\IndexController@Save');
 Route::post('/users/navigations/del','Admin\Navigations\IndexController@DelItem');
 Route::post('/users/navigations/moveto','Admin\Navigations\IndexController@MoveTo');
 Route::post('/users/navigations/getmodel','Admin\Navigations\IndexController@GetModel');
 Route::post('/users/navigations/langkey/exist', 'Admin\Navigations\IndexController@LangKeyExist');
 Route::post('/users/navigations/geturi', 'Admin\Navigations\IndexController@GetURI');
 Route::post('/users/navigations/saveuri','Admin\Navigations\IndexController@SaveURI');
 Route::post('/users/navigations/delteteuri', 'Admin\Navigations\IndexController@DelteteURI');
//endregion

//region 后台角色设置
 Route::post('/users/usersroles/getlist', 'Admin\UsersRoles\IndexController@GetList');
 Route::post('/users/usersroles/create', 'Admin\UsersRoles\IndexController@Create');
 Route::post('/users/usersroles/setinfo', 'Admin\UsersRoles\IndexController@SetInfo');
 Route::post('/users/usersroles/enable', 'Admin\UsersRoles\IndexController@Enable');
 Route::post('/users/usersroles/disable', 'Admin\UsersRoles\IndexController@Disable');
 Route::post('/users/usersroles/del', 'Admin\UsersRoles\IndexController@DelItem');
 Route::post('/users/usersroles/tree', 'Admin\UsersRoles\IndexController@Tree');
 Route::post('/users/usersroles/treesave', 'Admin\UsersRoles\IndexController@TreeSave');
 Route::post('/users/usersroles/getusersmembers', 'Admin\UsersRoles\IndexController@GetUsersANDMembers');
 Route::post('/users/usersroles/savemembers', 'Admin\UsersRoles\IndexController@SaveMembers');
//endregion

//region 企业菜单管理
 Route::post('/users/EnterpriseMenus/tree', 'Admin\EnterpriseMenus\IndexController@Tree');
 Route::post('/users/EnterpriseMenus/save', 'Admin\EnterpriseMenus\IndexController@Save');
 Route::post('/users/EnterpriseMenus/del','Admin\EnterpriseMenus\IndexController@DelItem');
 Route::post('/users/EnterpriseMenus/moveto','Admin\EnterpriseMenus\IndexController@MoveTo');
 Route::post('/users/EnterpriseMenus/getmodel','Admin\EnterpriseMenus\IndexController@GetModel');
 Route::post('/users/EnterpriseMenus/langkey/exist', 'Admin\EnterpriseMenus\IndexController@LangKeyExist');
 Route::post('/users/EnterpriseMenus/geturi', 'Admin\EnterpriseMenus\IndexController@GetURI');
 Route::post('/users/EnterpriseMenus/saveuri','Admin\EnterpriseMenus\IndexController@SaveURI');
 Route::post('/users/EnterpriseMenus/delteteuri', 'Admin\EnterpriseMenus\IndexController@DelteteURI');
//endregion

//EnterprisePageSettings
//region 企业页面设置
 Route::post('/users/EnterprisePageSettings/GetModel', 'Admin\EnterprisePageSettings\IndexController@GetModel');
 Route::post('/users/EnterprisePageSettings/SaveModel', 'Admin\EnterprisePageSettings\IndexController@SaveModel');
//endregion

//region AREA
Route::post('/users/area/getfullarea/{areaid}', 'Admin\Area\IndexController@GetFullArea');
//endregion

//region 企业菜单管理
 Route::post('/users/Enterprise/getlist', 'Admin\Enterprise\IndexController@GetList');
 Route::post('/users/Enterprise/getmodel', 'Admin\Enterprise\IndexController@GetModel');
//endregion

 // region企业后台账号
 Route::post('/users/Enterpriseusers/getlist', 'Admin\EnterpriseUsers\IndexController@GetList');
 Route::post('/users/Enterpriseusers/create', 'Admin\EnterpriseUsers\IndexController@Create');
 Route::post('/users/Enterpriseusers/exist','Admin\EnterpriseUsers\IndexController@Exist');
 Route::post('/users/Enterpriseusers/enable', 'Admin\EnterpriseUsers\IndexController@Enable');
 Route::post('/users/Enterpriseusers/disable','Admin\EnterpriseUsers\IndexController@Disable');
//endregion

//region 企业订单管理
 Route::post('/users/Enterpriseorders/getlist', 'Admin\EnterpriseOrders\IndexController@GetList');
//endregion

//region 企业账单管理
 Route::post('/users/Enterpriseordersaccounts/getlist', 'Admin\EnterpriseOrdersAccounts\IndexController@GetList');
//endregion

//region 服务期限管理
 Route::post('/users/Enterpriseservice/getlist', 'Admin\EnterpriseService\IndexController@GetList');
//endregion

//region 服务套餐管理
Route::post('/users/Enterpriseservicepackage/getlist', 'Admin\EnterpriseServicePackage\IndexController@GetList');
Route::post('/users/Enterpriseservicepackage/create', 'Admin\EnterpriseServicePackage\IndexController@Create');
Route::post('/users/Enterpriseservicepackage/update', 'Admin\EnterpriseServicePackage\IndexController@Update');
Route::post('/users/Enterpriseservicepackage/enable', 'Admin\EnterpriseServicePackage\IndexController@Enable');
Route::post('/users/Enterpriseservicepackage/disable', 'Admin\EnterpriseServicePackage\IndexController@Disable');
Route::post('/users/Enterpriseservicepackage/delete', 'Admin\EnterpriseServicePackage\IndexController@Delete');
//endregion

//region 会员管理
 Route::post('/users/Members/getlist', 'Admin\Members\IndexController@GetList');
//endregion
//region 会员订单管理
Route::post('/users/Membersorders/getlist', 'Admin\MembersOrders\IndexController@GetList');
//endregion

//region  会员平台接口
Route::post('/members/check', 'Members\Members\IndexController@Check');
Route::post('/members/refresh','Members\Members\IndexController@Refresh');
Route::post('/members/changepassword','Members\Members\IndexController@ChangePassword');
Route::post('/members/register','Members\Members\IndexController@Register');
Route::post('/members/exist','Members\Members\IndexController@Exist');
Route::post('/members/uploadface','Members\Members\IndexController@UploadFace');
Route::post('/members/changepassword','Members\Members\IndexController@ChangePassword');
Route::post('/members/saveinfo','Members\Members\IndexController@SaveInfo');
Route::post('/members/getinfo','Members\Members\IndexController@GetInfo');
//endregion

//region 企业订单
Route::post('/members/EnterpriseOrders/create', 'Members\EnterpriseOrders\IndexController@Create');
Route::post('/members/EnterpriseOrders/GetList', 'Members\EnterpriseOrders\IndexController@GetList');
Route::post('/members/EnterpriseOrders/GetDetails', 'Members\EnterpriseOrders\IndexController@GetDetails');
//endregion

//region 企业服务套餐
Route::post('/members/EnterpriseServicePackage/GetList', 'Members\EnterpriseServicePackage\IndexController@GetList');
//endregion

//region 企业
Route::post('/members/Enterprise/Infos', 'Members\Enterprise\IndexController@Infos');
Route::post('/members/Enterprise/Save', 'Members\Enterprise\IndexController@Save');
Route::post('/members/Enterprise/{field}/UploadImg', 'Members\Enterprise\IndexController@UploadImg')->where('field', 'LicensePicture|IDface|IDback');
//endregion

//region 企业简历库
Route::post('/members/Enterprise/Resume', 'Members\Enterprise\Resume\IndexController@Index');

//endregion

//region 企业相册
Route::any('/members/EnterprisePicture/Categorys', 'Members\EnterprisePicture\IndexController@Categorys');
Route::any('/members/EnterprisePicture/Pictures', 'Members\EnterprisePicture\IndexController@Pictures');
//endregion

//region 会员实名认证
Route::any('/members/MembersRealname/Save', 'Members\MembersRealname\IndexController@Save');
Route::any('/members/MembersRealname/Get', 'Members\MembersRealname\IndexController@Get');
Route::post('/members/MembersRealname/{field}/UploadImg', 'Members\MembersRealname\IndexController@UploadImg')->where('field', 'IDface|IDback');
//endregion

//region 企业页面设置
Route::any('/members/EnterprisePageSettings/GetModel', 'Members\EnterprisePageSettings\IndexController@GetModel');
//endregion

//region 在线简历
Route::post('/members/Resume/GetInfos', 'Members\Resume\IndexController@GetInfos');
Route::post('/members/Resume/SaveItems', 'Members\Resume\IndexController@SaveItems');
Route::post('/members/Resume/SaveWorkExperience', 'Members\Resume\IndexController@SaveWorkExperience');
Route::post('/members/Resume/DeleteWorkExperience', 'Members\Resume\IndexController@DeleteWorkExperience');
Route::post('/members/Resume/SaveEducationalExperience', 'Members\Resume\IndexController@SaveEducationalExperience');
Route::post('/members/Resume/DeleteEducationalExperience', 'Members\Resume\IndexController@DeleteEducationalExperience');
//endregion

//region 职位
Route::any('/members/EnterprisePosition/ChoicenessList', 'Members\EnterprisePosition\IndexController@ChoicenessList');
Route::any('/members/EnterprisePosition/GetList', 'Members\EnterprisePosition\IndexController@GetList');
Route::any('/members/EnterprisePosition/Infos', 'Members\EnterprisePosition\IndexController@Infos');
//endregion

//region 积分
Route::post('/members/Points', 'Members\Points\IndexController@Index');
Route::post('/members/Points/IncomeExpenses', 'Members\Points\IndexController@IncomeExpenses');
Route::post('/members/Points/DailyReward', 'Members\Points\IndexController@DailyReward');
Route::post('/members/Points/SignInInfos', 'Members\Points\IndexController@SignInInfos');
Route::post('/members/Points/SignIned', 'Members\Points\IndexController@SignIned');
//endregion

 //region 足迹
Route::post('/members/History', 'Members\History\IndexController@Index');
Route::post('/members/History/Delete', 'Members\History\IndexController@Delete');
Route::post('/members/History/Append', 'Members\History\IndexController@Append');
//endregion

//region 收藏
Route::post('/members/Collection', 'Members\Collection\IndexController@Index');
Route::post('/members/Collection/Delete', 'Members\Collection\IndexController@Delete');
Route::post('/members/Collection/Append', 'Members\Collection\IndexController@Append');
//endregion

//region 零工主页
Route::post('/members/OddJob', 'Members\OddJob\IndexController@Index');//翻页列表
Route::post('/members/OddJob/Save', 'Members\OddJob\IndexController@Save');
Route::post('/members/OddJob/GetModel', 'Members\OddJob\IndexController@GetModel');
Route::post('/members/OddJob/GetByID', 'Members\OddJob\IndexController@GetByID');
Route::post('/members/OddJob/AppendDynamic', 'Members\OddJob\IndexController@AppendDynamic');
//endregion

//region 零工需求
Route::post('/members/Moonlight', 'Members\Moonlight\IndexController@Index');
Route::post('/members/Moonlight/Save', 'Members\Moonlight\IndexController@Save');
Route::post('/members/Moonlight/GetByID', 'Members\Moonlight\IndexController@GetByID');
//endregion

//regin 钱包
Route::post('/members/Wallet', 'Members\Wallet\IndexController@Index');
Route::post('/members/Wallet/Balance', 'Members\Wallet\IndexController@Balance');
Route::post('/members/Wallet/Recharge', 'Members\Wallet\IndexController@Recharge');
Route::post('/members/Wallet/Withdraw', 'Members\Wallet\IndexController@Withdraw');
Route::post('/members/Wallet/GetWithdrawConfig', 'Members\Wallet\IndexController@GetWithdrawConfig');

//endregion



//regin 企业后台
Route::post('/enterprise/menus', 'Enterprise\IndexController@GetMenus');
//endregion

//region 企业后台个人中心
Route::post('/enterprise/usercenter/uploadface','Enterprise\UserCenter\IndexController@UploadFace');
Route::post('/enterprise/usercenter/changepassword', 'Enterprise\UserCenter\IndexController@ChangePassword' );
Route::post('/enterprise/usercenter/editinfo','Enterprise\UserCenter\IndexController@EditInfo' );
Route::post('/enterprise/usercenter/refresh','Enterprise\UserCenter\IndexController@Refresh');
//endregion

//region 系统配置菜单管理
 Route::post('/users/SystemConfigMenus/tree', 'Admin\SystemConfigMenus\IndexController@Tree');
 Route::post('/users/SystemConfigMenus/save', 'Admin\SystemConfigMenus\IndexController@Save');
 Route::post('/users/SystemConfigMenus/del','Admin\SystemConfigMenus\IndexController@DelItem');
 Route::post('/users/SystemConfigMenus/moveto','Admin\SystemConfigMenus\IndexController@MoveTo');
 Route::post('/users/SystemConfigMenus/getmodel','Admin\SystemConfigMenus\IndexController@GetModel');
 Route::post('/users/SystemConfigMenus/key/exist', 'Admin\SystemConfigMenus\IndexController@KeyExist');
//endregion

//region 配置管理
 Route::post('/users/ConfigManage/parentlist', 'Admin\ConfigManage\IndexController@ParentList');
 Route::post('/users/ConfigManage/getlist', 'Admin\ConfigManage\IndexController@GetList');
 Route::post('/users/ConfigManage/save', 'Admin\ConfigManage\IndexController@Save');
//endregion
