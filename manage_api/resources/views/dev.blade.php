@extends('layouts.dev')
@section('title', '开发工具')
@section('breadcrumb')
 <a href="{{URL::to('/dev')}}" class="current">@yield('title')</a>
@endsection
@section('content')
<div class="jumbotron">
	<h1 class="display-4">请选择需要生成的表进行操作</h1>
	<p class="lead">本操作会覆盖原有的文件，请确保选择的表未生成数据仓库。</p>
	<hr class="my-4">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>

				<th>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="checkbox" id="checkall" value="">
						<label class="form-check-label" for="checkall">选择全部</label>
					</div>
				</th>
				<th>
					操作
				</th>
				<th>说明</th>

			</tr>
		</thead>
		<tbody>
			@foreach($table_list as $table)
			<tr>
				<td>
					<label class="checkbox-inline">
						<input type="checkbox"  name="table_name" value="{{$table->Name}}" aria-label=""> {{$table->Name}}
					</label> </td>
						<td>
						 <a href="/dev/createform/{{$table->Name}}">生成vue表单</a>
						</td>
					<td>{{$table->Comment}}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3" class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
						<button type="button" class="btn btn-primary" id="Submit">生成</button>  <button type="button" class="btn btn-secondary" id="Submit1">仅生成model</button>
					</td>
				 
				</tr>
			</tbody>
		</table>
</div> 
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
		$('#checkall').on('click',function(){
  	 
  		 $('input[name="table_name"]').prop('checked',$(this).prop('checked'));
  		});
  $('#Submit').on('click',function(){
  
  	 var tables=[];
  	 $('input[name="table_name"]:checked').each(function(){
  	 		tables.push($(this).val());
  	 	});
  	 	if(tables.length>0)
  	 	{
  	 		var json={'table_name[]':tables,'_token':_token}
			var url='{{URL::to('/dev/createrepository')}}';
				$.ajax({
					url:url,
					type:'POST',
					cache:false,
					async:true,
					dataType:'html',
					data:json,			
					success:function(d){
				    alert(d);
					},
					error:function (XMLHttpRequest, textStatus, errorThrown) {
					     alert('请求出错'+XMLHttpRequest+'/'+textStatus);
					}
				});
  	 	}
  	 	else{
  	 		alert('至少选择一个表');
  	 		}
  	});
  	 $('#Submit1').on('click',function(){
  	 var tables=[];
  	 $('input[name="table_name"]:checked').each(function(){
  	 		tables.push($(this).val());
  	 	});
  	 	if(tables.length>0)
  	 	{
  	 		var json={'table_name[]':tables,'_token':_token}
			var url='{{URL::to('/dev/createrepository2')}}';
				$.ajax({
					url:url,
					type:'POST',
					cache:false,
					async:true,
					dataType:'html',
					data:json,			
					success:function(d){
				    alert(d);
					},
					error:function (XMLHttpRequest, textStatus, errorThrown) {
					     alert('请求出错'+XMLHttpRequest+'/'+textStatus);
					}
				});
  	 	}
  	 	else{
  	 		alert('至少选择一个表');
  	 		}
  	});
  
});
</script>
@endsection