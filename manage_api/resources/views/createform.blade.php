@extends('layouts.dev')
@section('title', '生成表单')
@section('breadcrumb')
<a href="{{URL::to('/dev')}}" >@yield('title')</a>
<a href="{{URL::to('/dev/createform'.$table_name)}}" class="current">@yield('title')</a>
@endsection
@section('content')
		<div class="container">
		<h1 class="display-4">生成vue表单</h1>
		<p class="lead">请修改设置表单的类型</p>
		<hr class="my-4">
		<form class="form-horizontal">
					<div class="form-group row">
						<label   class="col-sm-2 col-form-label">label</label>
				<label   class="col-sm-2 col-form-label">字段</label>
				<div class="col-sm-8">
				 组件类型
				</div>
				</div>
		@foreach($fields as $field)
				<div class="form-group row">
						<label  class="col-sm-2 col-form-label">{{$field->Comment}}</label>
				<label  class="col-sm-2 col-form-label">{{$field->Field}}</label>
				<div class="col-sm-8">
					 <div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="type_{{$field->Field}}" id="type_{{$field->Field}}_none" value="none">
					  <label class="form-check-label" for="type_{{$field->Field}}_none">无</label>
					</div>
				 <div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="type_{{$field->Field}}" id="type_{{$field->Field}}_hidden" value="hidden">
					  <label class="form-check-label" for="type_{{$field->Field}}_hidden">隐藏表单</label>
					</div>
				  <div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="type_{{$field->Field}}" id="type_{{$field->Field}}_input" value="input" checked>
					  <label class="form-check-label" for="type_{{$field->Field}}_input">单行输入框</label>
					</div>
				</div> 
				</div>
   
  @endforeach
  <div class="form-group row">
						<label  class="col-sm-2 col-form-label">表单代码</label>
		</div>				
  <div class="form-group row">
  	<textarea id="result" style="height:400px;" class="form-control"></textarea>
  </div>
   <div class="form-group row">
						<label  class="col-sm-2 col-form-label">model</label>
		</div>				
  <div class="form-group row">
  	<textarea id="modelresult" style="height:400px;" class="form-control"></textarea>
  </div>
   <div class="form-group row">
						<label  class="col-sm-2 col-form-label">视图模型</label>
		</div>
		<div class="form-group row">
  	<textarea id="view" style="height:400px;" class="form-control"></textarea>
  </div>		
  </form>
</div>
@endsection
@section('scripts')
<script>
  var fields={!!json_encode($fields)!!};
  $(document).ready(function(){
  	createForm();
  	$('input[type=radio]').on('change',createForm);
  });
  function createForm()
  { 
  	var code='';
  	code+='<el-form :model="ruleForm" size="default" label-width="120px" ref="formRef">'+"\r\n";
  	code+='	<el-row :gutter="35">'+"\r\n";
  	for(var i=0;i<fields.length;i++)
  	{
  		var type=$('input[name=type_'+fields[i].Field+']:checked').val();
  		//console.log(type);
  		switch(type)
			{
			    case 'input':
			        	code+='		<el-col :xs="24" :sm="18" :md="18" :lg="18" :xl="18" class="mb20">'+"\r\n";
					  	 	code+='		<el-form-item label="'+fields[i].Comment+'" :rules="[{ required: true, message: \'请输入'+fields[i].Comment+'\'}]" prop="'+fields[i].Field+'">'+"\r\n";
					  	 	code+='			<el-input v-model="ruleForm.'+fields[i].Field+'" placeholder="请输入'+fields[i].Comment+'" clearable v-on:keyup.enter="onSubmit(formRef)"></el-input>'+"\r\n";
					  	 	code+='			</el-form-item>'+"\r\n";
					  	 	code+='		</el-col>'+"\r\n";
			        break;
			    case 'none':
			         
			        break;
			     case 'hidden':
			         code+='		<el-input v-model="ruleForm.'+fields[i].Field+'" type="hidden"></el-input>'+"\r\n";
			        break;
			    default:
			    
			        break;
			}
		
  	}
  	code+='	</el-row>'+"\r\n";
  	code+='</el-form>'+"\r\n";
  	$('#result').val(code);
  	code='ruleForm:{'+"\r\n";
  	for(var i=0;i<fields.length;i++){
  		code+='		'+fields[i].Field+':\'\','+"\r\n";
  		
  	}
  	code+='		}'+"\r\n";
  		$('#modelresult').val(code);
  	code='';
  	
  		code='fields:{'+"\r\n";
  	for(var i=0;i<fields.length;i++){
  		code+='		'+fields[i].Field+':\''+fields[i].Comment+'\','+"\r\n";
  		
  	}
  	code+='		}'+"\r\n";
  	$('#view').val(code);
  		
  }
</script>
@endsection