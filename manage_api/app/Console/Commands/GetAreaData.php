<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\AreaRepository;
use App\Repositories\AreaDataRepository;
use App\Repositories\JobCategoryRepository;
use GuzzleHttp\Client;
class GetAreaData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetAreaData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步一些地域数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(AreaRepository $AreaRepository,AreaDataRepository $AreaDataRepository,JobCategoryRepository $JobCategoryRepository)
    {
    	
     
    	$list =$AreaRepository->all();
    	
    	foreach($list as $area)
    	{
    	  echo '正在处理'.$area->Name."\r\n";
				$http = new Client();
				$response = $http->get("https://api.map.baidu.com/geocoder/v2/?address=".$area->Name."&output=json&ak=YR65y102d5VroNuRzbYuE1TZnOVOnWin");
				$address_data = $response->getBody()->getContents();
				$json_data = json_decode($address_data,true);
				if($json_data['status']==0)
				{
				  
				  $area->Lng=$json_data['result']['location']['lng'];
    		 	$area->Lat=$json_data['result']['location']['lat'];
    		  $area->save();	
				}
    		usleep(1000);
    	}
    	 
     
      return 0;
    }
}
