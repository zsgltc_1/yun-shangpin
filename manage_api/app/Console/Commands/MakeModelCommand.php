<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Console\Commands\Creators\ModelCreator;

class MakeModelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
      protected $name = 'make:modelclass';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model class';
    /**
     * @var ModelCreator
     */
    protected $creator;

    /**
     * @var
     */
    protected $composer;
    /**
     * @param CriteriaCreator $creator
     */
    public function __construct(ModelCreator $creator)
    {
        parent::__construct();

        // Set the creator.
        $this->creator  = $creator;

        // Set the composer.
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get the arguments.
        $arguments = $this->argument();

        // Get the options.
        $options   = $this->option();

        // Write model.
        $this->writeModel($arguments, $options);

        // Dump autoload.
        $this->composer->dumpAutoloads();
    }
       /**
     * @param $arguments
     * @param $options
     */
    protected function writeModel($arguments, $options)
    {
       
        $model = $arguments['model'];
 
				$table_name      = $options['table_name'];

				$primaryKey_name      = $options['primaryKey_name'];

				$keyType_name      = $options['keyType_name'];
				$fileds     = $options['fileds'];
				$isincrementing  = $options['isincrementing'];
				$castslist= $options['castslist'];
        // Create 
        if($this->creator->create($model,$table_name,$primaryKey_name,$keyType_name,$fileds,$isincrementing,$castslist))
        {
        	 $this->info($fileds);
            // Information message.
            $this->info("Successfully  the model class");
        }
    }
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['model', InputArgument::REQUIRED, 'The repository name.']
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['table_name', null, InputOption::VALUE_OPTIONAL, 'The model name.', null],
            ['primaryKey_name', null, InputOption::VALUE_OPTIONAL, 'The model name.', null],
            ['keyType_name', null, InputOption::VALUE_OPTIONAL, 'The model name.', null],
            ['fileds', null, InputOption::VALUE_OPTIONAL, 'The model name.', null],
            ['isincrementing',true,InputOption::VALUE_OPTIONAL, 'The model name.', true],
            ['castslist',null,InputOption::VALUE_OPTIONAL, 'The model name.', true],
            
        ];
    }
}
