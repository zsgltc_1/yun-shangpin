<?php

namespace App\Console\Commands\Creators;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;
use Doctrine\Common\Inflector\Inflector;

/**
 * Class ModelCreator
 *
 * @package   App\Console\Commands\Creators
 */
class ModelCreator {
	
 /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var
     */
    protected $table_name;
    /**
     * @var
     */
    protected $primaryKey_name;
 
    /**
     * @var
     */
    protected $keyType_name;
    
     /**
     * @var
     */
    protected $fileds;
    
    /**
     * @var
     */
    protected $model;
    protected $isincrementing;
     protected $castslist;
    /**
     * @return mixed
     */
    public function getIsincrementing()
    {
        return $this->isincrementing;
    }
    /**
     * @param mixed 
     */
    public function setIsincrementing($isincrementing)
    {
        $this->isincrementing = $isincrementing;
    }
    
    /**
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }
    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }
     /**
     * @return mixed
     */
    public function getTable_name()
    {
        return $this->table_name;
    }

    /**
     * @param mixed $model
     */
    public function setTable_name($table_name)
    {
        $this->table_name = $table_name;
    }
     /**
     * @return mixed
     */
    public function getPrimaryKey_name()
    {
        return $this->primaryKey_name;
    }

    /**
     * @param mixed $model
     */
    public function setPrimaryKey_name($primaryKey_name)
    {
        $this->primaryKey_name = $primaryKey_name;
    }
     /**
     * @return mixed
     */
    public function getkeyType_name()
    {
        return $this->keyType_name;
    }

    /**
     * @param mixed $model
     */
    public function setKeyType_name($keyType_name)
    {
        $this->keyType_name = $keyType_name;
    }
     /**
     * @return mixed
     */
    public function getFileds()
    {
        return $this->fileds;
    }

    /**
     * @param mixed $model
     */
    public function setFileds($fileds)
     {
        $this->fileds = $fileds;
    }
     protected function createDirectory()
    {
        // Directory.
        $directory = $this->getDirectory();

        // Check if the directory exists.
        if(!$this->files->isDirectory($directory))
        {
            // Create the directory if not.
            $this->files->makeDirectory($directory, 0755, true);
        }
    }

    /**
     * Get the repository directory.
     *
     * @return mixed
     */
    protected function getDirectory()
    {
        // Get the directory from the config file.
        $directory = base_path(Config::get('repositories.model_path'));

        // Return the directory.
        return $directory;
    }
    /**
     * Get the path.
     *
     * @return string
     */
    protected function getPath()
    {
        // Path.
        $path = $this->getDirectory() . DIRECTORY_SEPARATOR . $this->model. '.php';

        // return path.
        return $path;
    }
    protected function createClass()
    {
        // Result.
        $result = $this->files->put($this->getPath(), $this->populateStub());

        // Return the result.
        return $result;
    }
     /**
     * Populate the stub.
     *
     * @return mixed
     */
    protected function populateStub()
    {
        // Populate data
        $populate_data = $this->getPopulateData();

        // Stub
        $stub = $this->getStub();

        // Loop through the populate data.
        foreach ($populate_data as $key => $value)
        {
            // Populate the stub.
            $stub = str_replace($key, $value, $stub);
        }

        // Return the stub.
        return $stub;
    }
     /**
     * Get the stub.
     *
     * @return string
     */
    protected function getStub()
    {
        // Stub
        $stub = $this->files->get($this->getStubPath() . "model.stub");

        // Return stub.
        return $stub;
    }
    /**
     * Get the stub path.
     *
     * @return string
     */
    protected function getStubPath()
    {
        // Stub path.
        $stub_path = __DIR__ . '/../../../../resources/stubs/';

        // Return the stub path.
        return $stub_path;
    }
     /**
     * Get the populate data.
     *
     * @return array
     */
    protected function getPopulateData()
    {
        // Repository namespace.
        $model_namespace = Config::get('repositories.model_namespace');

        // Repository class.
        $model_class     = $this->getModelName();

        // Model path.
        $model_path           = Config::get('repositories.model_path');

        
        // Populate data.
        $populate_data = [
            'model_namespace' => $model_namespace,
            'model_class'     => $model_class,
            'table_name'           => $this->getTable_name(),
            'primaryKey_name'           => $this->getPrimaryKey_name(),
            'keyType_name'           => $this->getkeyType_name(),
            'isincrementing'=>$this->getIsincrementing(),
            'fileds'           => $this->getFileds(),
            'castslist'=>$this->getCastsliste(),
        ];

        // Return populate data.
        return $populate_data;
    }
     protected function getModelName()
    {
        // Set model.
        $model      = $this->getModel();

        // Check if the model isset.
        if(isset($model) && !empty($model))
        {
            // Set the model name from the model option.
            $model_name = $model;
        }

        else
        {
            // Set the model name by the stripped repository name.
            $model_name = Inflector::singularize($this->stripRepositoryName());
        }

        // Return the model name.
        return $model_name;
    }
     /**
     * @param mixed $model
     */
    public function setCastsliste($castslist)
    {
        $this->castslist = $castslist;
    }
      /**
     * @return mixed
     */
    public function getCastsliste()
    {
        return $this->castslist;
    }
     /**
     * Create the repository.
     *
     * @param $repository
     * @param $model
     * @return int
     */
    public function create($model,$table_name,$primaryKey_name,$keyType_name,$fileds,$isincrementing,$castslist)
    {
       
        // Set the model.
        $this->setModel($model);
           
        $this->setTable_name($table_name);   
           
        $this->setPrimaryKey_name($primaryKey_name);  
        
        $this->setKeyType_name($keyType_name);
        $this->setIsincrementing($isincrementing);
        $this->setFileds ($fileds); 
        $this->setCastsliste($castslist);
        // Create the directory.
        $this->createDirectory();

        // Return result.
        return $this->createClass();
    }
  }