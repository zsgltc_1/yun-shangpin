<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersInRoles
 * @package App\Models
 */
class UsersInRoles extends Model
{
	protected $table='UsersInRoles';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['UsersID',
    									 'RolesID'];
	protected $casts=['ID'=>'string',
										'UsersID'=>'string',
										'RolesID'=>'string'];
}