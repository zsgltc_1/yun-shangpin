<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseService
 * @package App\Models
 */
class EnterpriseService extends Model
{
	protected $table='EnterpriseService';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'OrderSN',
    									 'EnterpriseID',
    									 'EnterpriseOrdersDetailsID',
    									 'StartDate',
    									 'EndDate',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string',
										'EnterpriseOrdersDetailsID'=>'string'];
}