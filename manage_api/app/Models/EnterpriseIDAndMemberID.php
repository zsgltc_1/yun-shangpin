<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseIDAndMemberID
 * @package App\Models
 */
class EnterpriseIDAndMemberID extends Model
{
	protected $table='EnterpriseIDAndMemberID';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['EnterpriseID',
    									 'MembersID',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string',
										'MembersID'=>'string'];
}