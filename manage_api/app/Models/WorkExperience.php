<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WorkExperience
 * @package App\Models
 */
class WorkExperience extends Model
{
	protected $table='WorkExperience';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['ResumeID',
    									 'CompanyName',
    									 'PositionName',
    									 'StartDate',
    									 'EndDate',
    									 'Contents',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'ResumeID'=>'string'];
}