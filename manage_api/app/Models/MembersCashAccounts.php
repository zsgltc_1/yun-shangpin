<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MembersCashAccounts
 * @package App\Models
 */
class MembersCashAccounts extends Model
{
	protected $table='MembersCashAccounts';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['MembersID',
    									 'Type',
    									 'SourceID',
    									 'Debit',
    									 'Credit',
    									 'Status',
    									 'Memo',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}