<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemConfig
 * @package App\Models
 */
class SystemConfig extends Model
{
	protected $table='SystemConfig';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'ParentID',
    									 'AllID',
    									 'Sort',
    									 'Orders',
    									 'Depth',
    									 'Key',
    									 'Text',
    									 'Value',
    									 'Description',
    									 'Type',
    									 'Options',
    									 'Max',
    									 'Min',
    									 'MaxLength',
    									 'Multiple',
    									 'Html',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'ParentID'=>'string'];
}