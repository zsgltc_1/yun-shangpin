<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OddJobDynamicPicture
 * @package App\Models
 */
class OddJobDynamicPicture extends Model
{
	protected $table='OddJobDynamicPicture';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['OddJobDynamicID',
    									 'FileName',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'OddJobDynamicID'=>'string'];
}