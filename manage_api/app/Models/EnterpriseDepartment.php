<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseDepartment
 * @package App\Models
 */
class EnterpriseDepartment extends Model
{
	protected $table='EnterpriseDepartment';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['EnterpriseID',
    									 'ParentID',
    									 'AllID',
    									 'Sort',
    									 'Orders',
    									 'Name',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string',
										'ParentID'=>'string'];
}