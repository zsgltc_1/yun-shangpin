<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterprisePictureCategory
 * @package App\Models
 */
class EnterprisePictureCategory extends Model
{
	protected $table='EnterprisePictureCategory';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['EnterpriseID',
    									 'Name',
    									 'Description',
    									 'Cover',
    									 'Sort',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string'];
}