<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseUsers
 * @package App\Models
 */
class EnterpriseUsers extends Model
{
	protected $table='EnterpriseUsers';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'EnterpriseID',
    									 'Name',
    									 'Avatar',
    									 'AccountName',
    									 'Password',
    									 'MobileTelephone',
    									 'Email',
    									 'QQ',
    									 'Wechat',
    									 'LoginCount',
    									 'LastIp',
    									 'LoginTime',
    									 'Roles',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string',
										'LoginTime'=>'string'];
}