<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MembersHistory
 * @package App\Models
 */
class MembersHistory extends Model
{
	protected $table='MembersHistory';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['MembersID',
    									 'Title',
    									 'URL',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}