<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Collection
 * @package App\Models
 */
class Collection extends Model
{
	protected $table='Collection';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['MembersID',
    									 'Type',
    									 'ResourceID',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string',
										'ResourceID'=>'string'];
}