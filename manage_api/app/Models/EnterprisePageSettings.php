<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterprisePageSettings
 * @package App\Models
 */
class EnterprisePageSettings extends Model
{
	protected $table='EnterprisePageSettings';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'Content',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string'];
}