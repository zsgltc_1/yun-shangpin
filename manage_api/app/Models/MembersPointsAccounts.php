<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MembersPointsAccounts
 * @package App\Models
 */
class MembersPointsAccounts extends Model
{
	protected $table='MembersPointsAccounts';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['MembersID',
    									 'Type',
    									 'SourceID',
    									 'Debit',
    									 'Credit',
    									 'Status',
    									 'Memo',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string',
										'SourceID'=>'string'];
}