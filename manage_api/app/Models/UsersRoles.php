<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersRoles
 * @package App\Models
 */
class UsersRoles extends Model
{
	protected $table='UsersRoles';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'Name',
    									 'Description',
    									 'AllowAppendMembers',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string'];
}