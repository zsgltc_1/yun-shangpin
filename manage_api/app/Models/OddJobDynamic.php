<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OddJobDynamic
 * @package App\Models
 */
class OddJobDynamic extends Model
{
	protected $table='OddJobDynamic';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'MembersID',
    									 'Content',
    									 'Location',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}