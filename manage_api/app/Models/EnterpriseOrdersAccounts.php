<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseOrdersAccounts
 * @package App\Models
 */
class EnterpriseOrdersAccounts extends Model
{
	protected $table='EnterpriseOrdersAccounts';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'OrderSN',
    									 'MembersID',
    									 'EnterpriseID',
    									 'Memo',
    									 'Debit',
    									 'Credit',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string',
										'EnterpriseID'=>'string'];
}