<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseDocuments
 * @package App\Models
 */
class EnterpriseDocuments extends Model
{
	protected $table='EnterpriseDocuments';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['EnterpriseID',
    									 'Title',
    									 'Content',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string'];
}