<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ResumeBuyLog
 * @package App\Models
 */
class ResumeBuyLog extends Model
{
	protected $table='ResumeBuyLog';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['EnterpriseID',
    									 'ResumeID',
    									 'OrderSN',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string',
										'ResumeID'=>'string'];
}