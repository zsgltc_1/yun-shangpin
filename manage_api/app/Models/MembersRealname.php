<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MembersRealname
 * @package App\Models
 */
class MembersRealname extends Model
{
	protected $table='MembersRealname';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'MembersID',
    									 'Name',
    									 'LinkmanPhone',
    									 'CertificatesType',
    									 'CertificateNo',
    									 'IDface',
    									 'IDback',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}