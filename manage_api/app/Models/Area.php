<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Area
 * @package App\Models
 */
class Area extends Model
{
	protected $table='Area';
	protected $primaryKey = 'ID';
	protected $keyType = 'decimal(18,6)';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['Name',
    									 'ParentID',
    									 'Depth',
    									 'AllID',
    									 'Sort',
    									 'Orders',
    									 'Lng',
    									 'Lat'];
	protected $casts=['ID'=>'string',
										'ParentID'=>'string'];
}