<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterprisePicture
 * @package App\Models
 */
class EnterprisePicture extends Model
{
	protected $table='EnterprisePicture';
	protected $primaryKey = 'ID';
	protected $keyType = 'int';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['EnterpriseID',
    									 'FileName',
    									 'EnterprisePictureCategoryID',
    									 'Status',
    									 'CreateTime',
    									 'Sort'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string',
										'EnterprisePictureCategoryID'=>'string'];
}