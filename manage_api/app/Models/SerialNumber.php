<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SerialNumber
 * @package App\Models
 */
class SerialNumber extends Model
{
	protected $table='SerialNumber';
	protected $primaryKey = 'ID';
	protected $keyType = 'int';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['Type',
    									 'Number',
    									 'DateTimeVal'];
	protected $casts=[];
}