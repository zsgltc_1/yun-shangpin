<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MembersInEnterprise
 * @package App\Models
 */
class MembersInEnterprise extends Model
{
	protected $table='MembersInEnterprise';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['EnterpriseID',
    									 'MembersID'];
	protected $casts=['ID'=>'string',
										'EnterpriseID'=>'string',
										'MembersID'=>'string'];
}