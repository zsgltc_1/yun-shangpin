<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OddJob
 * @package App\Models
 */
class OddJob extends Model
{
	protected $table='OddJob';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['MembersID',
    									 'Name',
    									 'Gender',
    									 'Wechat',
    									 'LinkmanPhone',
    									 'AreaAllID',
    									 'AreaID',
    									 'Area',
    									 'Addrees',
    									 'Skills',
    									 'Introduction',
    									 'WorkingHours',
    									 'Email',
    									 'Lng',
    									 'Lat',
    									 'Status',
    									 'CreateTime',
    									 'UpdateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string',
										'UpdateTime'=>'string'];
}