<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseOrders
 * @package App\Models
 */
class EnterpriseOrders extends Model
{
	protected $table='EnterpriseOrders';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'OrderSN',
    									 'MembersID',
    									 'SalesmanID',
    									 'Amount',
    									 'Memo',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string',
										'SalesmanID'=>'string'];
}