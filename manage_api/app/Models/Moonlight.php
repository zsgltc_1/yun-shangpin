<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Moonlight
 * @package App\Models
 */
class Moonlight extends Model
{
	protected $table='Moonlight';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'MembersID',
    									 'Name',
    									 'Description',
    									 'Picture',
    									 'Money',
    									 'Telephone',
    									 'AreaAllID',
    									 'AreaID',
    									 'Lat',
    									 'Lng',
    									 'Status',
    									 'CreateTime',
    									 'UpdateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string',
										'UpdateTime'=>'string'];
}