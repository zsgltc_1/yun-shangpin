<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MembersOrders
 * @package App\Models
 */
class MembersOrders extends Model
{
	protected $table='MembersOrders';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'OrderSN',
    									 'MembersID',
    									 'Type',
    									 'Amount',
    									 'Memo',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}