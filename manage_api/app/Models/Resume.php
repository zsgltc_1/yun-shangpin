<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Resume
 * @package App\Models
 */
class Resume extends Model
{
	protected $table='Resume';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'MembersID',
    									 'IsDefault',
    									 'ResumeName',
    									 'PersonalAdvantages',
    									 'JobType',
    									 'AreaAllID',
    									 'AreaID',
    									 'JobCity',
    									 'ExpectedSalary',
    									 'ExpectedPosition',
    									 'ExpectedIndustry',
    									 'JobStatus',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}