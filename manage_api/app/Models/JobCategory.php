<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class JobCategory
 * @package App\Models
 */
class JobCategory extends Model
{
	protected $table='JobCategory';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'ParentID',
    									 'Name',
    									 'AllID',
    									 'Depth',
    									 'Sort',
    									 'Orders',
    									 'BlueColla',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string'];
}