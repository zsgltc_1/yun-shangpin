<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EducationalExperience
 * @package App\Models
 */
class EducationalExperience extends Model
{
	protected $table='EducationalExperience';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['ResumeID',
    									 'SchoolName',
    									 'MajorName',
    									 'AcademicTitle',
    									 'StartDate',
    									 'EndDate',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'ResumeID'=>'string'];
}