<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersMenus
 * @package App\Models
 */
class UsersMenus extends Model
{
	protected $table='UsersMenus';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'ParentID',
    									 'AllID',
    									 'Sort',
    									 'Orders',
    									 'IsLink',
    									 'IsHide',
    									 'Component',
    									 'Depth',
    									 'LangKey',
    									 'Text',
    									 'Ico',
    									 'Path',
    									 'Redirect',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'ParentID'=>'string'];
}