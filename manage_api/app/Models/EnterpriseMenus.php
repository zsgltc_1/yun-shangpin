<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseMenus
 * @package App\Models
 */
class EnterpriseMenus extends Model
{
	protected $table='EnterpriseMenus';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'ParentID',
    									 'AllID',
    									 'Sort',
    									 'Orders',
    									 'IsLink',
    									 'IsHide',
    									 'Component',
    									 'Depth',
    									 'LangKey',
    									 'Text',
    									 'Ico',
    									 'Path',
    									 'Redirect',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'ParentID'=>'string'];
}