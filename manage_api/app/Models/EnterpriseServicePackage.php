<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseServicePackage
 * @package App\Models
 */
class EnterpriseServicePackage extends Model
{
	protected $table='EnterpriseServicePackage';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'Name',
    									 'OriginalPrice',
    									 'Price',
    									 'Months',
    									 'Description',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string'];
}