<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterprisePosition
 * @package App\Models
 */
class EnterprisePosition extends Model
{
	protected $table='EnterprisePosition';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['MembersID',
    									 'EnterpriseID',
    									 'Name',
    									 'Salary',
    									 'Experience',
    									 'Academic',
    									 'Label',
    									 'Description',
    									 'AreaAllID',
    									 'AreaID',
    									 'Telephone',
    									 'Addrees',
    									 'MoneyReward',
    									 'Type',
    									 'Poster',
    									 'Date',
    									 'Choiceness',
    									 'Lng',
    									 'Lat',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string',
										'EnterpriseID'=>'string'];
}