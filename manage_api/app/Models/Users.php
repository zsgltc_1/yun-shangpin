<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Users
 * @package App\Models
 */
class Users extends Model
{
	protected $table='Users';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'Number',
    									 'Name',
    									 'Avatar',
    									 'AccountName',
    									 'Password',
    									 'MobileTelephone',
    									 'Email',
    									 'QQ',
    									 'Wechat',
    									 'LoginCount',
    									 'LastIp',
    									 'LoginTime',
    									 'Roles',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'LoginTime'=>'string'];
}