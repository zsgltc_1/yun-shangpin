<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Members
 * @package App\Models
 */
class Members extends Model
{
	protected $table='Members';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'OpenID',
    									 'Nickname',
    									 'Name',
    									 'Avatar',
    									 'AccountName',
    									 'Password',
    									 'Birthday',
    									 'Gender',
    									 'Beginn',
    									 'PlaceResidence',
    									 'AreaID',
    									 'MobileTelephone',
    									 'Email',
    									 'QQ',
    									 'Wechat',
    									 'LastIp',
    									 'LoginTime',
    									 'CashBalance',
    									 'PointsBalance',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'LoginTime'=>'string'];
}