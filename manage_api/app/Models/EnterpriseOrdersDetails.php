<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnterpriseOrdersDetails
 * @package App\Models
 */
class EnterpriseOrdersDetails extends Model
{
	protected $table='EnterpriseOrdersDetails';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'OrderSN',
    									 'Name',
    									 'Memo',
    									 'Price',
    									 'Num',
    									 'SetSum',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string'];
}