<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersRolesInMenus
 * @package App\Models
 */
class UsersRolesInMenus extends Model
{
	protected $table='UsersRolesInMenus';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = true;
	public $timestamps = false;
	protected $fillable=['RolesID',
    									 'MenusID',
    									 'Value'];
	protected $casts=['ID'=>'string',
										'RolesID'=>'string',
										'MenusID'=>'string',
										'Value'=>'string'];
}