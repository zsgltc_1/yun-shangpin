<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Pay_Log
 * @package App\Models
 */
class Pay_Log extends Model
{
	protected $table='Pay_Log';
	protected $primaryKey = 'ID';
	protected $keyType = 'decimal(18,2)';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'PaySN',
    									 'OrderSN',
    									 'MembersID',
    									 'Type',
    									 'PaymentMode',
    									 'TradeStatus',
    									 'Status',
    									 'CreateTime',
    									 'TotalAmount'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}