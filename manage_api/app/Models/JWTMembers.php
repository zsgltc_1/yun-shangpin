<?php
namespace App\Models;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class JWTMembers extends Authenticatable implements JWTSubject
{
	use Notifiable;
	protected $table = 'Members';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'OpenID',
    									 'Nickname',
    									 'Name',
    									 'Avatar',
    									 'AccountName',
    									 'Password',
    									 'Birthday',
    									 'Gender',
    									 'Beginn',
    									 'PlaceResidence',
    									 'AreaID',
    									 'MobileTelephone',
    									 'Email',
    									 'QQ',
    									 'Wechat',
    									 'LastIp',
    									 'LoginTime',
    									 'CashBalance',
    									 'PointsBalance',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'LoginTime'=>'string'];
	protected $hidden = [
		'Password',
	];
	public function getAuthPassword()
	{
		return $this->attributes['Password'];
	}
	public function getRememberTokenName()
	{
		return null;
	}
	/**
	 * Get the identifier that will be stored in the subject claim of the JWT.
	 *
	 * @return mixed
	 */
	public function getJWTIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Return a key value array, containing any custom claims to be added to the JWT.
	 *
	 * @return array
	 */
	public function getJWTCustomClaims()
	{
		return [];
	}
}
