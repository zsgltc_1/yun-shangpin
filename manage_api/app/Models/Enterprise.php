<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Enterprise
 * @package App\Models
 */
class Enterprise extends Model
{
	protected $table='Enterprise';
	protected $primaryKey = 'ID';
	protected $keyType = 'bigint';
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable=['ID',
    									 'MembersID',
    									 'Name',
    									 'AreaID',
    									 'AreaAllID',
    									 'Addrees',
    									 'Representative',
    									 'LicenseNo',
    									 'LicensePicture',
    									 'IDface',
    									 'IDback',
    									 'LinkmanName',
    									 'LinkmanPhone',
    									 'Bank',
    									 'TaxNo',
    									 'Lng',
    									 'Lat',
    									 'Status',
    									 'CreateTime'];
	protected $casts=['ID'=>'string',
										'MembersID'=>'string'];
}