<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class CollectionRepository
 * @package App\Repositories
 */
class CollectionRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Collection';
    }
    public function deleteWhere($where)
    {
    	$this->applyCriteria();
      $model = $this->model;
       foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }
        return $model->delete();
    }
}