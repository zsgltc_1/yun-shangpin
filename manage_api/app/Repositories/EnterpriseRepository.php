<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\DB;

/**
 * Class EnterpriseRepository
 * @package App\Repositories
 */
class EnterpriseRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Enterprise';
    }
    /**
    * 后台专用
    */
    public function GetList($kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [], $columns = ['Enterprise.*','Area.Name as Area','Members.Nickname as Nickname'])
    {
        $this->applyCriteria();
        $model = $this->model;
        
         $model = $model->leftjoin('Area', function ($join){
            $join->on('Area.ID', '=', 'Enterprise.AreaID');

        });
          $model = $model->leftjoin('Members', function ($join){
            $join->on('Members.ID', '=', 'Enterprise.MembersID');

        });
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
							$query->where('Enterprise.Name', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.Addrees', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.Representative', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.LicenseNo', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.LinkmanName', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.LinkmanPhone', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.Bank', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.TaxNo', 'like', '%' . $kw . '%')
							->orWhere('Area.Name', 'like', '%' . $kw . '%')
							->orWhere('Members.Nickname', 'like', '%' . $kw . '%')
							
							;
            });
        }
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }

        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }

        return $model->paginate($perPage, $columns);
    }
    
   public function Get_List($MembersID,$AreaID,$Lat,$Lng,$kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [])
   {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('Collection', function ($join)use($MembersID){
            $join->on('Collection.ResourceID', '=', 'Enterprise.ID')
            ->where('Collection.MembersID','=',$MembersID)->where('Collection.Type','=',2);

        });
        $model=$model->where('Enterprise.AreaAllID','like','%,' . $AreaID . ',%'); 
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
							$query->where('Enterprise.Name', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.Addrees', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.Representative', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.LicenseNo', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.LinkmanName', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.LinkmanPhone', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.Bank', 'like', '%' . $kw . '%')
							->orWhere('Enterprise.TaxNo', 'like', '%' . $kw . '%')
							;
            });
        }
       foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }
    
        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }
        $prefix=config('database.connections.'.config('database.default').'.prefix');
        $columns=['Enterprise.*','Collection.ID AS CollectionID',DB::raw('getdistance('.$Lat.','.$Lng.','.$prefix.'Enterprise.Lat,'.$prefix.'Enterprise.Lng)  as Distance')];
       return $model->paginate($perPage, $columns);
   	
   }
    
}