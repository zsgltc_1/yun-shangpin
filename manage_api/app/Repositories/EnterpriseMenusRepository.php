<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterpriseMenusRepository
 * @package App\Repositories
 */
class EnterpriseMenusRepository extends _treeRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterpriseMenus';
    }
    public function RolesAllByOrder($RolesID, $orders = 'Orders', $sort = 'asc', $columns = array('UsersMenus.*', 'UsersRolesInMenus.MenusID as Checked'))
    {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('UsersRolesInMenus', function ($join) use ($RolesID) {
            $join->on('UsersMenus.ID', '=', 'UsersRolesInMenus.MenusID')->where('UsersRolesInMenus.RolesID', '=', $RolesID);
    
        });
        return $model->orderBy($orders, $sort)->get($columns);
    }
    public function GetMenus($UsersID)
    {
    	  $this->applyCriteria();
        $model = $this->model;
        $prefix = config('database.connections.' . config('database.default') . '.prefix');
        $whereRaw = "(ID in(select  MenusID from " . $prefix . "UsersRolesInMenus where  RolesID=1593700352) OR ID in(select  MenusID from " . $prefix . "UsersRolesInMenus  where  RolesID in (select RolesID  from " . $prefix . "UsersInRoles where  UsersID=" . $UsersID . "))
        )";
    	    $model = $model->whereRaw($whereRaw);
    	   return $model->orderBy('Orders', 'asc')->orderBy('Sort', 'asc')->get();
    }
}
