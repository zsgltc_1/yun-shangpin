<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterpriseAuditLogRepository
 * @package App\Repositories
 */
class EnterpriseAuditLogRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterpriseAuditLog';
    }
}