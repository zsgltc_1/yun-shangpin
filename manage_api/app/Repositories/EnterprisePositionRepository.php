<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\DB;

/**
 * Class EnterprisePositionRepository
 * @package App\Repositories
 */
class EnterprisePositionRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterprisePosition';
    }
     public function Infos($MembersID,$ID,$Lat,$Lng)
    {
    	  $prefix=config('database.connections.'.config('database.default').'.prefix');
    	  $this->applyCriteria();
    	  $model = $this->model;
    	   $model = $model->leftjoin('Collection', function ($join)use($MembersID){
            $join->on('Collection.ResourceID', '=', 'EnterprisePosition.ID')
            ->where('Collection.MembersID','=',$MembersID)->where('Collection.Type','=',1);

        });
        $model=$model->where('ID','=',$ID);
        return $model->first(['EnterprisePosition.*','Collection.ID AS CollectionID',DB::raw('getdistance('.$Lat.','.$Lng.','.$prefix.'EnterprisePosition.Lat,'.$prefix.'EnterprisePosition.Lng)  as Distance')]);
    }
    public function ChoicenessList($MembersID,$AreaID,$Lat,$Lng)
    {
    	  $prefix=config('database.connections.'.config('database.default').'.prefix');
    	  $this->applyCriteria();
    	  $model = $this->model;
         $model = $model->leftjoin('Collection', function ($join)use($MembersID){
            $join->on('Collection.ResourceID', '=', 'EnterprisePosition.ID')
            ->where('Collection.MembersID','=',$MembersID)->where('Collection.Type','=',1);

        });
        $model=$model->where('EnterprisePosition.Choiceness','=',1);
        $model=$model->where('EnterprisePosition.AreaAllID','like','%,' . $AreaID . ',%'); 
        $model = $model->orderBy('Distance', 'ASC');
        return $model->get(['EnterprisePosition.*','Collection.ID AS CollectionID',DB::raw('getdistance('.$Lat.','.$Lng.','.$prefix.'EnterprisePosition.Lat,'.$prefix.'EnterprisePosition.Lng)  as Distance')]);
    }
     public function GetList($MembersID,$AreaID,$Lat,$Lng,$kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [])
    {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('Collection', function ($join)use($MembersID){
            $join->on('Collection.ResourceID', '=', 'EnterprisePosition.ID')
            ->where('Collection.MembersID','=',$MembersID)->where('Collection.Type','=',1);

        });
        $model=$model->where('EnterprisePosition.AreaAllID','like','%,' . $AreaID . ',%'); 
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
    						$query->where('EnterprisePosition.Name', 'like', '%' . $kw . '%')
    						->orWhere('EnterprisePosition.Salary', 'like', '%' . $kw . '%')
    						->orWhere('EnterprisePosition.Experience', 'like', '%' . $kw . '%')
    						->orWhere('EnterprisePosition.Label', 'like', '%' . $kw . '%')
    						->orWhere('EnterprisePosition.Academic', 'like', '%' . $kw . '%')
    						->orWhere('EnterprisePosition.Description', 'like', '%' . $kw . '%')
    						->orWhere('EnterprisePosition.Addrees', 'like', '%' . $kw . '%');
            });
        }
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }
    
        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }
         $prefix=config('database.connections.'.config('database.default').'.prefix');
         $columns=['EnterprisePosition.*','Collection.ID AS CollectionID',DB::raw('getdistance('.$Lat.','.$Lng.','.$prefix.'EnterprisePosition.Lat,'.$prefix.'EnterprisePosition.Lng)  as Distance')];
        return $model->paginate($perPage, $columns);
    }
}