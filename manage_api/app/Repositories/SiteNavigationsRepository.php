<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class SiteNavigationsRepository
 * @package App\Repositories
 */
class SiteNavigationsRepository extends _treeRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\SiteNavigations';
    }
}