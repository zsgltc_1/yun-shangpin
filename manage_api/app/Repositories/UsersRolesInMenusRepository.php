<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class UsersRolesInMenusRepository
 * @package App\Repositories
 */
class UsersRolesInMenusRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\UsersRolesInMenus';
    }
    public function DeleteByRolesID($RolesID)
    {
        return $this->model->where('RolesID', '=', $RolesID)->delete();
    }
    public function HasPermissions($MenuID,$UsersID)
    {
    	$this->applyCriteria();
		  $model = $this->model;
		  $model=$model->where('UsersRolesInMenus.MenuID', '=', $MenuID);  
		  $model = $model->join('UsersInRoles', function ($join)use($UsersID){
			     $join->on('UsersRolesInMenu.RolesID', '=', 'UsersInRoles.RolesID')->where('UsersInRoles.UsersID','=',$UsersID)->orWhere('UsersRolesInMenus.RolesID','=','394098546636365318');
		   });
 
       return $model->exists();
		  
    }
}