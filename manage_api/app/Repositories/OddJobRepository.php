<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\DB;

/**
 * Class OddJobRepository
 * @package App\Repositories
 */
class OddJobRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\OddJob';
    }
    
     public function GetByID($ID)
     {
     	$this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('Members', function ($join){
            $join->on('Members.ID', '=', 'OddJob.MembersID');
        });
       $model = $model->where('ID','=',$ID);
       return  $model->first(['OddJob.*','Members.Avatar']);
    }
    
     public function GetByMembersID($MembersID)
     {
     	$this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('Members', function ($join){
            $join->on('Members.ID', '=', 'OddJob.MembersID');
        });
       $model = $model->where('MembersID','=',$MembersID);
       return  $model->first(['OddJob.*','Members.Avatar']);
    }
    
   public function GetList($MembersID,$AreaID,$Lat,$Lng,$kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [])
   {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('Collection', function ($join)use($MembersID){
            $join->on('Collection.ResourceID', '=', 'OddJob.ID')
            ->where('Collection.MembersID','=',$MembersID)->where('Collection.Type','=',4);

        });
        $model = $model->leftjoin('Members', function ($join){
            $join->on('Members.ID', '=', 'OddJob.MembersID');
           
        });
        $model=$model->where('OddJob.AreaAllID','like','%,' . $AreaID . ',%'); 
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
								$query->where('OddJob.Name', 'like', '%' . $kw . '%')
    						->orWhere('OddJob.LinkmanPhone', 'like', '%' . $kw . '%')
    						->orWhere('OddJob.Addrees', 'like', '%' . $kw . '%')
    						->orWhere('OddJob.Skills', 'like', '%' . $kw . '%')
    						->orWhere('OddJob.Introduction', 'like', '%' . $kw . '%')
    						->orWhere('OddJob.WorkingHours', 'like', '%' . $kw . '%')
    						->orWhere('OddJob.Email', 'like', '%' . $kw . '%');
            });
        }
       foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }
    
        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }
        $prefix=config('database.connections.'.config('database.default').'.prefix');
        $columns=['OddJob.*','Collection.ID AS CollectionID',DB::raw('getdistance('.$Lat.','.$Lng.','.$prefix.'OddJob.Lat,'.$prefix.'OddJob.Lng)  as Distance'),'Members.Avatar'];
       return $model->paginate($perPage, $columns);
   	
   }
    
}