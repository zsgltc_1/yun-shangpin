<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class UsersRepository
 * @package App\Repositories
 */
class UsersRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Users';
    }
    public function NextNumber($length = 8)
    {
        $this->applyCriteria();
        $model = $this->model;
        $MaxNumber = $model->max('Number');
        if (empty($MaxNumber)) {
            $MaxNumber = 0;
        }
        $MaxNumber = (int)$MaxNumber;
        $MaxNumber++;
        return str_pad($MaxNumber, $length, '0', STR_PAD_LEFT);
    }
    public function GetList($kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [], $columns = ['*'],$pageName='page')
    {
        $this->applyCriteria();
        $model = $this->model;
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
                $query->where('Number', 'like', '%' . $kw . '%')
                    ->orWhere('Name', 'like', '%' . $kw . '%')
                    ->orWhere('AccountName', 'like', '%' . $kw . '%')
                    ->orWhere('MobileTelephone', 'like', '%' . $kw . '%')
                    ->orWhere('Email', 'like', '%' . $kw . '%')
                    ->orWhere('QQ', 'like', '%' . $kw . '%')
                    ->orWhere('Wechat', 'like', '%' . $kw . '%');
            });
        }
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }

        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }

        return $model->paginate($perPage, $columns,$pageName);
    }
    public function GetUsersANDMembers($RolesID)
    {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('UsersInRoles', function ($join) use ($RolesID) {
            $join->on('UsersInRoles.UsersID', '=', 'Users.ID')->where('UsersInRoles.RolesID', '=', $RolesID);
        });
        return $model->get(['Users.*','UsersInRoles.UsersID as Joined']);
    }
    

}