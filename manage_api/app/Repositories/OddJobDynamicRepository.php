<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class OddJobDynamicRepository
 * @package App\Repositories
 */
class OddJobDynamicRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\OddJobDynamic';
    }
}