<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class OddJobDynamicPictureRepository
 * @package App\Repositories
 */
class OddJobDynamicPictureRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\OddJobDynamicPicture';
    }
}