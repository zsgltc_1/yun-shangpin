<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class MembersPointsAccountsRepository
 * @package App\Repositories
 */
class MembersPointsAccountsRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\MembersPointsAccounts';
    }
    /**
    * 积分明细
    */
    
    public function GetListByMembersID($MembersID,$perPage = 25)
    {
     	 $this->applyCriteria();
       $model = $this->model;
       $model = $model->where('MembersID', '=', $MembersID);
       $model = $model->where('Status', '=', 1);
       $model = $model->orderBy('CreateTime', 'desc');
       return $model->paginate($perPage, ['*']);
    }
    /**
    * 总收入
    */
    public function GetSumDebit($MembersID)
    {
    	$this->applyCriteria();
      $model = $this->model;
      $model = $model->where('MembersID', '=', $MembersID);
      $model = $model->where('Status', '=', 1);
      return $model->sum('Debit');
    }
    /**
    * 总支出
    */
    public function GetSumCredit($MembersID)
    {
    	$this->applyCriteria();
      $model = $this->model;
      $model = $model->where('MembersID', '=', $MembersID);
      $model = $model->where('Status', '=', 1);
      return $model->sum('Credit');
    }
    /*
    * 每日奖励
    */
    public function DailyReward($MembersID,$Debit=20)
    {
    	$StartTime=strtotime("today");
    	$EndTime=strtotime("+1 day");
    	$this->applyCriteria();
      $model = $this->model;
      $model = $model->where('MembersID', '=', $MembersID);
      $model = $model->where('Status', '=', 1);
     // $model = $model->whereBetween('CreateTime'[$StartTime,$EndTime-1]);
       $model = $model->where('CreateTime', '>=', $StartTime);
       $model = $model->where('CreateTime', '<', $EndTime);
      if(!$model->exists())
      {
         	return $this->create(
         	[
         	'MembersID'=>$MembersID,
         	'Type'=>0,
         	'SourceID'=>0,
         	'Debit'=>$Debit,
         	'Credit'=>0,
         	'Status'=>1,
         	'Memo'=>'每日签到获得积分',
         	'CreateTime'=>time(),
         	]
         	);
         	
      }else{
      	
      	return false;
      	}
    }
    public function SignIned($MembersID)
    {
    	$StartTime=strtotime("today");
    	$EndTime=strtotime("+1 day");
    	$this->applyCriteria();
      $model = $this->model;
      $model = $model->where('MembersID', '=', $MembersID);
      $model = $model->where('Status', '=', 1);
      $model = $model->where('CreateTime', '>=', $StartTime);
      $model = $model->where('CreateTime', '<', $EndTime);
      return $model->exists();
    }
    public function  SignInInfos($MembersID)
    {
      $model = $this->model;
      $model = $model->where('MembersID', '=', $MembersID);
      $model = $model->where('Status', '=', 1);
      $model = $model->where('Type', '=', 0);
      $count=$model->count();
      $sum= $model->sum('Debit');
      $StartTime=strtotime("-7 day");
      $model = $model->where('CreateTime', '>=', $StartTime);
      $list=$model->get();
      return ['count'=>$count,'sum'=>$sum,'list'=>$list];
    }
}