<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterpriseOrdersRepository
 * @package App\Repositories
 */
class EnterpriseOrdersRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterpriseOrders';
    }
    public function GetList($kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [], $columns = ['EnterpriseOrders.*','m1.Nickname as Members','m2.Nickname as Salesman'])
    {
        $this->applyCriteria();
        $model = $this->model;
        $model=$model ->join('Members AS m1', function ($join) {
             $join->on('EnterpriseOrders.MembersID', '=', 'm1.ID');
        });
         $model=$model ->leftjoin('Members AS m2', function ($join) {
             $join->on('EnterpriseOrders.SalesmanID', '=', 'm2.ID');
        });
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
    						$query->where('EnterpriseOrders.OrderSN', 'like', '%' . $kw . '%')
    						->orWhere('m1.Nickname', 'like', '%' . $kw . '%')
    						->orWhere('EnterpriseOrders.Memo', 'like', '%' . $kw . '%')
    						 ;
            });
        }
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }

        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }

        return $model->paginate($perPage, $columns);
    }
}
