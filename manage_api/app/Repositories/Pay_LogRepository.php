<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class Pay_LogRepository
 * @package App\Repositories
 */
class Pay_LogRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Pay_Log';
    }
}