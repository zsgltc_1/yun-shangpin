<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class UsersRolesRepository
 * @package App\Repositories
 */
class UsersRolesRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\UsersRoles';
    }
     public function GetList($kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [], $columns = ['*'])
    {
        $this->applyCriteria();
        $model = $this->model;
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
                $query->where('Name', 'like', '%' . $kw . '%')
                    ->orWhere('Description', 'like', '%' . $kw . '%');
            });
        }
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }

        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }

        return $model->paginate($perPage, $columns);
    }
    public function GetByUsersID($UsersID)
    {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->join('UsersInRoles', function ($join) use ($UsersID) {
            $join->on('UsersInRoles.RolesID', '=', 'UsersRoles.ID')->where('UsersInRoles.UsersID', '=', $UsersID);
        });
        return $model->get(['UsersRoles.*']);
    }
    public function GetListANDUserID($UsersID)
    {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('UsersInRoles', function ($join) use ($UsersID) {
            $join->on('UsersRoles.ID', '=', 'UsersInRoles.RolesID')->where('UsersInRoles.UsersID', '=', $UsersID);
        });
        $model = $model->where('UsersRoles.Status', '=', 0);
        $model = $model->where('UsersRoles.AllowAppendMembers', '=', 1);
        return $model->get(['UsersRoles.*', 'UsersInRoles.UsersID']);
    }

}