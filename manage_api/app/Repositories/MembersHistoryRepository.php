<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class MembersHistoryRepository
 * @package App\Repositories
 */
class MembersHistoryRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\MembersHistory';
    }
    public function GetListByMembersID($MembersID,$perPage = 25)
    {
     	 $this->applyCriteria();
       $model = $this->model;
       $model = $model->where('MembersID', '=', $MembersID);
       $model = $model->orderBy('CreateTime', 'desc');
       return $model->paginate($perPage, ['*']);
    }
    
}