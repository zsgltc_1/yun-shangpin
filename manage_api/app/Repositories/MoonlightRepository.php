<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\DB;
/**
 * Class MoonlightRepository
 * @package App\Repositories
 */
class MoonlightRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Moonlight';
    }
    public function GetList($MembersID,$AreaID,$Lat,$Lng,$kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [])
   {
        $this->applyCriteria();
        $model = $this->model;
        $model = $model->leftjoin('Collection', function ($join)use($MembersID){
            $join->on('Collection.ResourceID', '=', 'Moonlight.ID')
            ->where('Collection.MembersID','=',$MembersID)->where('Collection.Type','=',3);

        });
        $model=$model->where('Moonlight.AreaAllID','like','%,' . $AreaID . ',%'); 
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
    						$query->where('Moonlight.Name', 'like', '%' . $kw . '%')
    						->orWhere('Moonlight.Description', 'like', '%' . $kw . '%')    					 
    						->orWhere('Moonlight.Telephone', 'like', '%' . $kw . '%');
            });
        }
       foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }
    
        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);
    
            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);
    
                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }
        $prefix=config('database.connections.'.config('database.default').'.prefix');
        $columns=['Moonlight.*','Collection.ID AS CollectionID',DB::raw('getdistance('.$Lat.','.$Lng.','.$prefix.'Moonlight.Lat,'.$prefix.'Moonlight.Lng)  as Distance')];
       return $model->paginate($perPage, $columns);
   	
   }
   
    
}