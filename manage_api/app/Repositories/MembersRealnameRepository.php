<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class MembersRealnameRepository
 * @package App\Repositories
 */
class MembersRealnameRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\MembersRealname';
    }
}