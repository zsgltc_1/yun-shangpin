<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class ResumeRepository
 * @package App\Repositories
 */
class ResumeRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Resume';
    }
    public function GetList($EnterpriseID,$isBuyed,$AreaID,$kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [], $columns = ['Resume.*','ResumeBuyLog.OrderSN as OrderSN'])
    {
     	$this->applyCriteria();
      $model = $this->model;
      $model = $model->leftjoin('ResumeBuyLog', function ($join)use($EnterpriseID){
        $join->on('ResumeBuyLog.ResumeID', '=', 'Resume.ID')->where('ResumeBuyLog.EnterpriseID','=',$EnterpriseID);

      });
      if($isBuyed)
      {
      	$model = $model->where('ResumeBuyLog.OrderSN','<>',NULL);
      }else{
      	$model = $model->where('ResumeBuyLog.OrderSN','=',NULL);
      }
       if (!empty($AreaID)) {
       	$model = $model->where('Resume.AreaAllID','like','%,' . $AreaID . ',%');
       	}
       if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
							$query->where('Resume.ResumeName', 'like', '%' . $kw . '%')
							->orWhere('Resume.PersonalAdvantages', 'like', '%' . $kw . '%')
							->orWhere('Resume.JobType', 'like', '%' . $kw . '%')
							->orWhere('Resume.JobCity', 'like', '%' . $kw . '%')
							->orWhere('Resume.ExpectedSalary', 'like', '%' . $kw . '%')
							->orWhere('Resume.ExpectedPosition', 'like', '%' . $kw . '%')
							 
							;
            });
        }
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }

        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }

        return $model->paginate($perPage, $columns);
    }
}