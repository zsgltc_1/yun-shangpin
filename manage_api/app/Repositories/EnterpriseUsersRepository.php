<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterpriseUsersRepository
 * @package App\Repositories
 */
class EnterpriseUsersRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterpriseUsers';
    }
    public function GetList($kw = '', $where = [], $orWhere = [], $perPage = 25, $orderBy = [], $columns = ['EnterpriseUsers.*','e1.Name as EnterpriseName'])
    {
        $this->applyCriteria();
        $model = $this->model;
        $model=$model ->join('Enterprise AS e1', function ($join) {
             $join->on('EnterpriseUsers.EnterpriseID', '=', 'e1.ID');
        });
        if (!empty($kw)) {
            $model = $model->where(function ($query) use ($kw) {
        					$query->where('EnterpriseUsers.Name', 'like', '%' . $kw . '%')
        					->orWhere('e1.Name', 'like', '%' . $kw . '%')
        					->orWhere('EnterpriseUsers.AccountName', 'like', '%' . $kw . '%')
        					 ;
            });
        }
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->where($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->where($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->where($field, '=', $search);
                }
            }
            else {
                $model = $model->where($field, '=', $value);
            }
        }

        foreach ($orWhere as $field => $value) {
            if ($value instanceof \Closure) {
                $model = $model->orWhere($value);

            }
            elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = $model->orWhere($field, $operator, $search);

                }
                elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = $model->orWhere($field, '=', $search);
                }
            }
            else {
                $model = $model->orWhere($field, '=', $value);
            }
        }
        foreach ($orderBy as $field => $sort) {
            $model = $model->orderBy($field, $sort);
        }

        return $model->paginate($perPage, $columns);
    }
}
