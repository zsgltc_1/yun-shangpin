<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class WorkExperienceRepository
 * @package App\Repositories
 */
class WorkExperienceRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\WorkExperience';
    }
}