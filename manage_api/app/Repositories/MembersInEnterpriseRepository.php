<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class MembersInEnterpriseRepository
 * @package App\Repositories
 */
class MembersInEnterpriseRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\MembersInEnterprise';
    }
}