<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class ResumeBuyLogRepository
 * @package App\Repositories
 */
class ResumeBuyLogRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\ResumeBuyLog';
    }
}