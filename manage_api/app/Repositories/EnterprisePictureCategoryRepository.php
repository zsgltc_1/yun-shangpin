<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterprisePictureCategoryRepository
 * @package App\Repositories
 */
class EnterprisePictureCategoryRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterprisePictureCategory';
    }
}