<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class JobCategoryRepository
 * @package App\Repositories
 */
class JobCategoryRepository extends _treeRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\JobCategory';
    }
}