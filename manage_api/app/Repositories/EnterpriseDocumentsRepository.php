<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterpriseDocumentsRepository
 * @package App\Repositories
 */
class EnterpriseDocumentsRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterpriseDocuments';
    }
}