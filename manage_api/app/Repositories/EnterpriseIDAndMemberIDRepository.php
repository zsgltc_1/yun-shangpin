<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterpriseIDAndMemberIDRepository
 * @package App\Repositories
 */
class EnterpriseIDAndMemberIDRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterpriseIDAndMemberID';
    }
}