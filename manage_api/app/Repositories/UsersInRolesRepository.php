<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class UsersInRolesRepository
 * @package App\Repositories
 */
class UsersInRolesRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\UsersInRoles';
    }
     public function DeleteByRolesID($RolesID)
    {
        return $this->model->where('RolesID', '=', $RolesID)->delete();
    }
    public function DeleteByUsersID($UsersID)
    {
        return $this->model->where('UsersID', '=', $UsersID)->delete();
    }
}