<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterprisePageSettingsRepository
 * @package App\Repositories
 */
class EnterprisePageSettingsRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterprisePageSettings';
    }
}