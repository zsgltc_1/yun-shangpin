<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class AreaRepository
 * @package App\Repositories
 */
class AreaRepository extends _treeRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Area';
    }
    public function GetFullArea($ID)
    {
    		$this->applyCriteria();
        $model = $this->model->where('ID', $ID)->first();
        if(empty($model))
        {
        	return '';
        }
        $array = array();
      
        while (!empty($model)) {
            $array[] = $model->Name;
            $model = $this->model->where('ID', $model->ParentID)->first();

        }
        $array = array_reverse($array);
        return  join(' ', $array) ;
    }
   
}