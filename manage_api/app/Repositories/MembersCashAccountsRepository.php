<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class MembersCashAccountsRepository
 * @package App\Repositories
 */
class MembersCashAccountsRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\MembersCashAccounts';
    }
     /**
    * ��ϸ
    */
    
    public function GetListByMembersID($MembersID,$perPage = 25)
    {
     	 $this->applyCriteria();
       $model = $this->model;
       $model = $model->where('MembersID', '=', $MembersID);
       $model = $model->where('Status', '=', 1);
       $model = $model->orderBy('CreateTime', 'desc');
       return $model->paginate($perPage, ['*']);
    }
     public function Balance($MembersID)
    {
    	$this->applyCriteria();
      $model = $this->model;
      $model = $model->where('MembersID', '=', $MembersID);
      $model = $model->where('Status', '=', 1);
      $Debit=$model->sum('Debit');
      $Debit=$Debit==null?0:$Debit;
      $Credit=$model->sum('Credit');
      $Credit=$Credit==null?0:$Credit;
      return floatval($Debit) - floatval($Credit);
    }
}