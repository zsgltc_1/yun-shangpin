<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class EnterpriseOrdersDetailsRepository
 * @package App\Repositories
 */
class EnterpriseOrdersDetailsRepository extends _appRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\EnterpriseOrdersDetails';
    }
}