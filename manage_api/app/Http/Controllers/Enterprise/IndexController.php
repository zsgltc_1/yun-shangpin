<?php

namespace App\Http\Controllers\Enterprise;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseMenusRepository;

class IndexController extends Controller
{
   public function __construct()
	{
		$this->middleware('auth:enterprise');

	}
	/* ��ȡ�˵� */
  public function GetMenus(EnterpriseMenusRepository $EnterpriseMenusRepository, Request $request)
     {
     	$this->list=$EnterpriseMenusRepository->GetMenu();
     	$data = $this->GetMenuList(0);
     	return response()->json(['code' => 0, 'data' => $data, 'type' => 'adminMenu']);
     }
     protected function GetMenuList($ParentID)
     {
     	$list = [];
     	foreach ($this->list as $item) {
     		if ($item->ParentID == $ParentID) {
     			$list[] = [
     				"path" => $item->Path,
     				"name" => $item->Path,
     				"component" => $item->Component,
     				"redirect" => $item->Redirect,
     				"props" => true,
     				"meta" => [
     					"title" => "message.menus." . $item->LangKey,
     					"isLink" => $item->IsLink == 1 ? true : "",
     					"isKeepAlive" => false,
     					"isAffix" => false,
     					"isIframe" => false,
     					"roles" => ["admin", "common"],
     					"icon" => $item->Ico,
     					"isHide" => $item->IsHide == 1 ? true : ""
     				],
     				'children' => $this->GetMenuList($item->ID)
     			];
     		}
     	}
     	return $list;
     }
}
