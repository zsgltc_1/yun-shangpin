<?php

namespace App\Http\Controllers\Enterprise\UserCenter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Repositories\EnterpriseUsersRepository;

class IndexController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:enterprise', ['except' => ['check']]);
    }

    /*
     *  上传自己的头像
     */
    public function UploadFace(Request $request, EnterpriseUsersRepository $EnterpriseUsersRepository)
    {
    	$enterpriseuser = auth('enterprise')->user();
    	if ($request->hasFile('file')) {

    		$file = $request->file('file');

    		$ext = $file->getClientOriginalExtension();
    		$realPath = $file->getRealPath();
    		$type = $file->getClientMimeType();

    		$filename = uniqid() . '.' . $ext;
    		//Log::info($filename);
    		if (in_array(strtolower($file->extension()), ['jpeg', 'jpg', 'gif', 'gpeg', 'png'])) {
    			// 使用我们新建的uploads本地存储空间（目录）
    			//这里的upload是配置文件的名称
    			$bool = Storage::disk('oss')->put("/avatar/enterprise/" . $filename, file_get_contents($realPath));
    			//判断是否创建成功
    			if (!$bool) {
    				return response()->json(['code' => 1, 'message' => '头像上传失败']);
    			}
    			else {
    				if (trim($enterpriseuser->Avatar) != '') {
    					//删除旧的头像
    					Storage::disk('oss')->delete("/avatar/enterprise/" . $enterpriseuser->Avatar);
    				}
    				$EnterpriseUsersRepository->update(['Avatar' => $filename], $enterpriseuser->ID);
    				return response()->json(['code' => 0, 'message' => '头像上传成功', 'result' => $filename]);
    			}
    		}
    		else {
    			return response()->json(['code' => 1, 'message' => '请选择图片文件']);
    		}
    	}

    }

    /**
     *  修改自己的密码
     */
    public function ChangePassword(Request $request, EnterpriseUsersRepository $EnterpriseUsersRepository)
    {
    	$enterpriseuser = auth('enterprise')->user();
    	$credentials = [
    		'password' => trim($request->input('OLDPassword')),
    		'accountname' => $enterpriseuser->AccountName
    	];

    	if ($token = auth('enterprise')->attempt($credentials)) {

    		$EnterpriseUsersRepository->update(['Password' => bcrypt(trim($request->input('Password')))], $enterpriseuser->ID);
    		return response()->json([
    			'code' => 0,
    			'access_token' => $token,
    			'token_type' => 'bearer',
    			'expires_in' => auth('enterprise')->factory()->getTTL() * 60,
    			'result' => auth('enterprise')->user(),
    			'message' => '密码修改成功'
    		]);

    	}
    	else {
    		return response()->json(['code' => 1, 'message' => '旧密码错误']);
    	}

    }

    /* 修改资料 */
    public function EditInfo(EnterpriseUsersRepository $EnterpriseUsersRepository, Request $request)
    {
    	$requestData = [
    		'AccountName' => trim($request->input('AccountName')),
    		'MobileTelephone' => trim($request->input('MobileTelephone')),
    		'Email' => trim($request->input('Email')),
    		'QQ' => trim($request->input('QQ')),
    		'Wechat' => trim($request->input('Wechat')),
    	];
    	$enterpriseuser = auth('enterprise')->user();
    	$ID = $enterpriseuser->ID;
    	$EnterpriseUsersRepository->update($requestData, $ID);
    	return response()->json(['code' => 0, 'message' => '完成资料设置']);
    }

    /**
     * Refresh a token.
     * 刷新token，如果开启黑名单，以前的token便会失效。
     * 值得注意的是用上面的getToken再获取一次Token并不算做刷新，两次获得的Token是并行的，即两个都可用。
     * @return \Illuminate\Http\JsonResponse
     */
    public function Refresh()
    {
    	return $this->respondWithToken(auth('enterprise')->refresh());
    }
    protected function respondWithToken($token)
    {
        $userInfo = auth('enterprise')->user();
        // $userInfo->Avatar=$userInfo->Avatar==''?$userInfo->Avatar:$this->ToImageURL('/avatar/enterprise/'.$userInfo->Avatar);
    	return response()->json([
    		'code' => 0,
    		'access_token' => $token,
    		'token_type' => 'bearer',
    		'expires_in' => auth('enterprise')->factory()->getTTL() * 60,
    		'result' => auth('enterprise')->user(),
    	]);
    }
}
