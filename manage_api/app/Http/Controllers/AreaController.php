<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SiteNavigationsRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Repositories\AreaRepository;
/**
 * 
 */
class AreaController extends Controller
{

	public function __construct()
	{
	 
	}
	 
	/**
	* 全部地域数据
	*/
	 public function Index(AreaRepository $AreaRepository,  Request $request)
	 {
	 	  	return response()->json(['code' => 0, 'result' => $AreaRepository->GetMenu()]);
	 }
	 public function GetModel(AreaRepository $AreaRepository,  Request $request)
	 {
	 	  $ID=$request->input('ID');
	 	  return response()->json(['code' => 0, 'result' => $AreaRepository->GetModel($ID)]);
	 }
	  public function GetListByParentID(AreaRepository $AreaRepository,  Request $request)
	 {
	 	  $ID=$request->input('ID');
	 	  return response()->json(['code' => 0, 'result' => $AreaRepository->GetListByParentID($ID)]);
	 }
	 public function GetFullArea(AreaRepository $AreaRepository,  Request $request)
	 {
	 	  $ID=$request->input('ID');
	 	  return response()->json(['code' => 0, 'result' => $AreaRepository->GetFullArea($ID)]);
	 }
	  public function GetList(AreaRepository $AreaRepository,  Request $request)
	  {
	  	 $result=[];
	  	 for($i=65;$i<91;$i++)
	  	 {
	  	  $first=chr($i);
	  	  $list=$AreaRepository->GetALL(['First'=>$first],[],['Orders'=>'asc']);
	  	  if(count($list->toArray())>0){
	  	  	$result[]=$list;
	  	  	}
	  	 }
	 
	  	 return response()->json(['code' => 0, 'result' => $result]);
	  }
}