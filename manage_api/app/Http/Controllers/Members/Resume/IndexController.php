<?php

namespace App\Http\Controllers\Members\Resume;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResumeRepository;
use App\Repositories\WorkExperienceRepository;
use App\Repositories\EducationalExperienceRepository;
use App\Repositories\AreaRepository;
use Illuminate\Support\Facades\Validator;
/**
* 在线简历
*/
class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
		
		public function GetInfos(Request $request,
		ResumeRepository $ResumeRepository
		,WorkExperienceRepository $WorkExperienceRepository
		,EducationalExperienceRepository $EducationalExperienceRepository)
		{
			$user = auth('members')->user();
			$OrderSN=$request->input('OrderSN');
			$result=$ResumeRepository->findBy('MembersID',$user->ID);
			if(empty($result))
			{
				$ResumeRepository->create(['ID'=>$this->nextid(),'MembersID'=>$user->ID,'IsDefault'=>1,'ResumeName'=>'默认简历','CreateTime'=>time()]);
				$result=$ResumeRepository->findBy('MembersID',$user->ID);
			}
			$result->WorkExperience=$WorkExperienceRepository->findAllBy('ResumeID',$result->ID);
			$result->EducationalExperience=$EducationalExperienceRepository->findAllBy('ResumeID',$result->ID);
			return response()->json(['code' => 0, 'result' => $result]);
		}
		public function SaveItems(Request $request,ResumeRepository $ResumeRepository,AreaRepository $AreaRepository)
		{
			 $rules = [
            'item' => ['required','regex:/^PersonalAdvantages|JobType|JobCity|ExpectedSalary|ExpectedPosition|ExpectedIndustry|JobStatus|AreaID$/'],
        ];
       $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'message' => $validator->errors()->first()]);
        }
			$item=$request->input('item');
			$value=$request->input('value');
			$user = auth('members')->user();
		  $result=$ResumeRepository->findBy('MembersID',$user->ID);
			if(empty($result))
			{
				$ResumeRepository->create(['ID'=>$this->nextid(),'MembersID'=>$user->ID,'IsDefault'=>1,'ResumeName'=>'默认简历','CreateTime'=>time()]);
				$result=$ResumeRepository->findBy('MembersID',$user->ID);
			}
			$result->$item=$value;
			if($item=='AreaID')
			{
					$area=$AreaRepository->find($value);
					if(!empty($area))
					{
						$result->AreaAllID=$area->AllID;
					}
			}
			$result->save();
			return response()->json(['code' => 0, 'message' => '保存成功！']);
		}
		public function SaveWorkExperience(Request $request,WorkExperienceRepository $WorkExperienceRepository,ResumeRepository $ResumeRepository)
		{
			$user = auth('members')->user();
			$ID=$request->input('ID','0');
			$Data=[
			'ResumeID'=>trim($request->input('ResumeID')),
			 'CompanyName'=>trim($request->input('CompanyName')),
			 'PositionName'=>trim($request->input('PositionName')),
			 'StartDate'=>trim($request->input('StartDate')),
			 'EndDate'=>trim($request->input('EndDate')),
			 'Contents'=>trim($request->input('Contents')),
			];
			$result=$ResumeRepository->findBy('MembersID',$user->ID);
			if($Data['ResumeID']!=$result->ID)
			{
				return response()->json(['code' => 1, 'message' => '简历ID错误！']);
			}
			if($ID=='0')
			{
				$Data['CreateTime']=time();
				$ID=$WorkExperienceRepository->create($Data);
				
			}else{
				$WorkExperienceRepository->update($Data,$ID);
				}
			return response()->json(['code' => 0, 'message' => '保存成功！','result'=>$ID]);
		}
		public function DeleteWorkExperience(Request $request,WorkExperienceRepository $WorkExperienceRepository,ResumeRepository $ResumeRepository)
		{
			$user = auth('members')->user();
			$ID=$request->input('ID','0');
			$model=$WorkExperienceRepository->find($ID);
			if(empty($model))
			{
				return response()->json(['code' => 1, 'message' => '找不到数据！']);
			}
			$result=$ResumeRepository->findBy('MembersID',$user->ID);
			if($model->ResumeID!=$result->ID)
			{
				return response()->json(['code' => 1, 'message' => '简历ID错误！']);
			}
			$model->delete();
			
			return response()->json(['code' => 0, 'message' => '删除成功！']);
		}
		public function SaveEducationalExperience(Request $request,EducationalExperienceRepository $EducationalExperienceRepository,ResumeRepository $ResumeRepository)
		{
			$user = auth('members')->user();
			$ID=$request->input('ID','0');
			$Data=[
			'ResumeID'=>trim($request->input('ResumeID')),
			 'SchoolName'=>trim($request->input('SchoolName')),
			 'MajorName'=>trim($request->input('MajorName')),
			 'StartDate'=>trim($request->input('StartDate')),
			 'EndDate'=>trim($request->input('EndDate')),
			 'AcademicTitle'=>trim($request->input('AcademicTitle')),
			];
			$result=$ResumeRepository->findBy('MembersID',$user->ID);
			if($Data['ResumeID']!=$result->ID)
			{
				return response()->json(['code' => 1, 'message' => '简历ID错误！']);
			}
			if($ID=='0')
			{
				$Data['CreateTime']=time();
				$ID=$EducationalExperienceRepository->create($Data);
				
			}else{
				$EducationalExperienceRepository->update($Data,$ID);
				}
			return response()->json(['code' => 0, 'message' => '保存成功！','result'=>$ID]);
		}
		public function DeleteEducationalExperience(Request $request,EducationalExperienceRepository $EducationalExperienceRepository,ResumeRepository $ResumeRepository)
		{
			$user = auth('members')->user();
			$ID=$request->input('ID','0');
			$model=$EducationalExperienceRepository->find($ID);
			if(empty($model))
			{
				return response()->json(['code' => 1, 'message' => '找不到数据！']);
			}
			$result=$ResumeRepository->findBy('MembersID',$user->ID);
			if($model->ResumeID!=$result->ID)
			{
				return response()->json(['code' => 1, 'message' => '简历ID错误！']);
			}
			$model->delete();
			
			return response()->json(['code' => 0, 'message' => '删除成功！']);
		}
}
