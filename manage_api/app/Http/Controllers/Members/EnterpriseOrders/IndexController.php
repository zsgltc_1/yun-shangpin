<?php

namespace App\Http\Controllers\Members\EnterpriseOrders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseServicePackageRepository;
use App\Repositories\EnterpriseOrdersRepository;
use App\Repositories\EnterpriseOrdersDetailsRepository;
use App\Repositories\EnterpriseOrdersAccountsRepository;
use App\Repositories\EnterpriseRepository;
use App\Repositories\SerialNumberRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Log;
/**
* 企业订单
*/
class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
		
		public function GetList(EnterpriseOrdersRepository $EnterpriseOrdersRepository,Request $request)
		{
			$pagesize = $request->input('pagesize', '25');
		  $kw = $request->input('kw', '');
		  $user = auth('members')->user();
		  $where=['MembersID'=>$user->ID];
		  $result = $EnterpriseOrdersRepository->GetList(addslashes($kw), $where, [], $pagesize, ['CreateTime' => 'desc'], ['*']);
		  return response()->json(['code' => 0, 'result' => $result]);
		}
		public function GetDetails(Request $request,EnterpriseOrdersRepository $EnterpriseOrdersRepository,EnterpriseOrdersDetailsRepository $EnterpriseOrdersDetailsRepository,EnterpriseOrdersAccountsRepository $EnterpriseOrdersAccountsRepository)
		{
			$user = auth('members')->user();
			$OrderSN=$request->input('OrderSN');
			$result=$EnterpriseOrdersRepository->findBy('OrderSN',$OrderSN);
			if(empty($result) || $result->MembersID!=$user->ID)
			{
				 return response()->json(['code' => 1, 'message' => '没有找到订单']);
			}
			$result->OrdersDetails=$EnterpriseOrdersDetailsRepository->findAllBy('OrderSN',$OrderSN);
			$result->OrdersAccounts=$EnterpriseOrdersAccountsRepository->findAllBy('OrderSN',$OrderSN);
			return response()->json(['code' => 0, 'result' => $result]);
		}
		//创建订单
		public function Create(
		EnterpriseServicePackageRepository $EnterpriseServicePackageRepository
		,EnterpriseOrdersRepository $EnterpriseOrdersRepository
		,EnterpriseOrdersDetailsRepository $EnterpriseOrdersDetailsRepository
		,EnterpriseOrdersAccountsRepository $EnterpriseOrdersAccountsRepository
		,EnterpriseRepository $EnterpriseRepository
		,SerialNumberRepository $SerialNumberRepository
		,Request $request)
		{
			 $user = auth('members')->user();
			 $PackageID=$request->input('PackageID');
			 $Num=intval($request->input('Num'));
			 if($Num<1)
			 {
			 	 return ['code' => 1, 'message' => '数量不能小于1！'];
			 	}
			 $Package=$EnterpriseServicePackageRepository->find($PackageID);
			 if(empty($Package) || $Package->Status==1)
			 {
			   return ['code' => 1, 'message' => '套餐不存在！'];
			 }
			 if($Package->Price==0 && $EnterpriseRepository->existsWhere(['MembersID'=>$user->ID]))
			 {
			 	 return ['code' => 1, 'message' => '当前套餐仅新人可以体验！'];
			 }
			 if($Package->Price==0 && $Num > 1)
			 {
			 	 return ['code' => 1, 'message' => '体验套餐只能购买一个！'];
			 }
		  $Orders=[
				'ID'=>$this->nextid(),
				'OrderSN'=>$SerialNumberRepository->GetNumber(1,6),
				'MembersID'=>$user->ID,
				'SalesmanID'=>'0',
				'Amount'=>0,
				'Memo'=>'购买企业服务',
				'Status'=>0,
				'CreateTime'=>time(),
		  ];
		  $OrdersDetails=[
		  'ID'=>$this->nextid(),
			 'OrderSN'=>$Orders['OrderSN'],
			 'Name'=>$Package->Name,
			 'Memo'=>$Package->Description,
			 'Price'=>$Package->Price,
			 'Num'=>$Num,
			 'SetSum'=>$Package->Price * $Num,
			 'Status'=>0,
			 'CreateTime'=>time(),
		  ];
		  $Orders['Amount']=$OrdersDetails['SetSum'];
		 
		  $OrdersAccounts=[
		   'ID'=>$this->nextid(),
			 'OrderSN'=>$Orders['OrderSN'],
			 'MembersID'=>$user->ID,
			 'EnterpriseID'=>0,
			 'Memo'=>$Orders['Memo'],
			 'Debit'=>$Orders['Amount'],
			 'Credit'=>0,
			 'Status'=>0,
			 'CreateTime'=>time(),
		  ];
		  
			DB::beginTransaction();
			try {
				  if($Orders['Amount']==0)
		      {
		      	$Orders['Status']=1;
		      	$OrdersAccounts['Status']=1;
		      }
		      //检查企业是否存在，不存在就创建
		      $Enterprise=$EnterpriseRepository->findBy('MembersID',$user->ID);
		      if(empty($Enterprise))
		      {
		      	 $Enterprise=[
						  'ID'=>$this->nextid(),
							 'MembersID'=>$user->ID,
							 'Name'=>'',
							 'AreaID'=>0,
							 'Addrees'=>'',
							 'Representative'=>'',
							 'LicenseNo'=>'',
							 'LicensePicture'=>'',
							 'IDface'=>'',
							 'IDback'=>'',
							 'LinkmanName'=>'',
							 'LinkmanPhone'=>'',
							 'Bank'=>'',
							 'TaxNo'=>'',
							 'Status'=>0,
							 'CreateTime'=>time(),
						  ];
		      	 $EnterpriseRepository->create($Enterprise);	
		    	}
		    	$OrdersAccounts['EnterpriseID']=$Enterprise['ID'];
			    $EnterpriseOrdersRepository->create($Orders);
			    $EnterpriseOrdersDetailsRepository->create($OrdersDetails);
		      $EnterpriseOrdersAccountsRepository->create($OrdersAccounts);
					DB::commit();
					return response()->json(['code' => 0, 'message' => '下单成功！']);
			}
			catch (Exception $e) {
				DB::rollBack();
				Log::error($e->getMessage());
				return ['code' => 1, 'message' => '下单失败！'];
			}
		}
		
}
