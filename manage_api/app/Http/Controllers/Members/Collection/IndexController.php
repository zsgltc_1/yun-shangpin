<?php

namespace App\Http\Controllers\Members\Collection;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CollectionRepository;
use App\Repositories\EnterprisePositionRepository;
use App\Repositories\EnterpriseRepository;
use App\Repositories\MoonlightRepository;
use App\Repositories\OddJobRepository;
class IndexController extends Controller
{
    public function __construct()
	 {
			$this->middleware('auth:members');
	 }
	  public function Index(Request $request
	  ,EnterpriseRepository $EnterpriseRepository
	  ,MoonlightRepository $MoonlightRepository
	  ,OddJobRepository $OddJobRepository
	  ,EnterprisePositionRepository $EnterprisePositionRepository)
	 {
	 	 $user = auth('members')->user();
	 	 $pagesize = $request->input('pagesize', '25');
	 	 $Type = $request->input('Type', '0');
	 	 $Lng=$request->input('Lng','0');
		 $Lat=$request->input('Lat','0');
	 	 switch($Type)
	 	 {
	 	 	 case '1'://职位
	 	 	 $where=[
		 	 	 [
		 	 	 'Collection.ID','!=',NULL
		 	 	 ],
	 	 	 ];
	 	 	 $result = $EnterprisePositionRepository->GetList($user->ID,'0',$Lat,$Lng,'', $where,[], $pagesize,  ['Collection.CreateTime'=>'DESC']);
	 	 	 break;
	 	 	 case '2'://企业
	 	 	  $where=[
		 	 	 [
		 	 	 'Collection.ID','!=',NULL
		 	 	 ],
	 	 	 ];
	 	 	 $result = $EnterpriseRepository->Get_List($user->ID,'0',$Lat,$Lng,'', $where,[], $pagesize,  ['Collection.CreateTime'=>'DESC']);
	 	 	 break;
	 	 	 case '3'://兼职
	 	 	  $where=[
		 	 	 [
		 	 	 'Collection.ID','!=',NULL
		 	 	 ],
	 	 	 ];
	 	 	 $result = $MoonlightRepository->GetList($user->ID,'0',$Lat,$Lng,'', $where,[], $pagesize,  ['Collection.CreateTime'=>'DESC']);
	 	 	 break;
	 	 	 case '4'://零工
	 	 	  $where=[
		 	 	 [
		 	 	 'Collection.ID','!=',NULL
		 	 	 ],
	 	 	 ];
	 	 	 $result = $OddJobRepository->GetList($user->ID,'0',$Lat,$Lng,'', $where,[], $pagesize,  ['Collection.CreateTime'=>'DESC']);
	 	 	 break;
	 	   default:
	 	    	return response()->json(['code' => 1, 'message'=>'参数错误']);
	 	  	
	 	 }
	 	 
	 	 return response()->json(['code' => 0, 'result' => $result]);
	 }
	 public function Delete(Request $request,CollectionRepository $CollectionRepository)
	 {
	 	 $ID= $request->input('ID');
	 	  $user = auth('members')->user();
	 	 $model=$CollectionRepository->find($ID);
	 	 if(!empty($model) &&  $model->MembersID==$user->ID)
	 	 {
	 	 	$result =  $model->delete();
	 	 	return response()->json(['code' => 0, 'result' => $result,'message'=>'操作成功']);
	 	 }
	 	 else{
	 	 	return response()->json(['code' => 1, 'message'=>'操作失败']);
	 	 	}
	 }
	 public function Append(Request $request,CollectionRepository $CollectionRepository)
	 {
	 	  $user = auth('members')->user();
	 	  $Type=trim($request->input('Type'));
	 	  $ResourceID=trim($request->input('ResourceID'));
	 	 if($CollectionRepository->existsWhere(['Type' => $Type,'ResourceID'=>$ResourceID,'MembersID'=>$user->ID]))
	 	 {
	 	 	 $CollectionRepository->deleteWhere(['Type' => $Type,'ResourceID'=>$ResourceID,'MembersID'=>$user->ID]);
	 	 	 return response()->json(['code' => 0, 'result' => false,'message'=>'操作成功']);
	 	 }else{
		 	 	$data=[
		 	  'MembersID'=>$user->ID,
		 	  'Type'=>trim($request->input('Type')),
		 	  'ResourceID'=>trim($request->input('ResourceID')),
		 	  'CreateTime'=>time(),
		 	  	'Status'=>0,
		 	  ];
		 	  $result =  $CollectionRepository->create($data);
		 	 return response()->json(['code' => 0, 'result' => true,'message'=>'操作成功']);
	 	 	}
	 	  
	 }
}
