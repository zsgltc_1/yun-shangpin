<?php

namespace App\Http\Controllers\Members\Enterprise\Resume;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResumeRepository;
use App\Repositories\EnterpriseRepository;
use App\Repositories\MembersInEnterpriseRepository;
use App\Http\Controllers\Members\EnterpriseController
/**
* 企业简历库
*/
class IndexController extends EnterpriseController
{
	  public function __construct(EnterpriseRepository $EnterpriseRepository,MembersInEnterpriseRepository $MembersInEnterpriseRepository)
		{
			 parent::__construct($EnterpriseRepository,$MembersInEnterpriseRepository);
		}
	  /**
	  *  翻页获取简历库
	  */
		public function Index(Request $request,ResumeRepository $ResumeRepository)
		{
		  if(empty($this->Enterprise))
		  {
		  	return response()->json(['code'=>1,'message' => '您还未拥有企业！']);	
		  }
			$isBuyed=$request->input('isBuyed');
			$isBuyed=$isBuyed=='true';
			$pagesize = $request->input('pagesize', '25');
		  $kw = $request->input('kw', '');
		  $AreaID = $request->input('AreaID', '');
		  $result = $EnterpriseOrdersRepository->GetList($this->Enterprise->ID,$isBuyed,addslashes($AreaID),addslashes($kw), $where, [], $pagesize, ['CreateTime' => 'desc'], ['*']);
		  return response()->json(['code' => 0, 'result' => $result]);
		}
}
