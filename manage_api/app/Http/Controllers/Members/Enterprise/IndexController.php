<?php

namespace App\Http\Controllers\Members\Enterprise;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseRepository;
use App\Repositories\AreaRepository;
use Storage;
/**
* 企业认证
*/
class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
		public function Infos(EnterpriseRepository $EnterpriseRepository,Request $request)
		{
			 $user = auth('members')->user();
			 $Enterprise=$EnterpriseRepository->findBy('MembersID',$user->ID);
			 if(!empty($Enterprise))
			 {
			 	$Enterprise->LicensePicture=  empty($Enterprise->LicensePicture)?'':$this->ToImageURL('/enterprise/LicensePicture/'.$Enterprise->LicensePicture);
			 	$Enterprise->IDface=  empty($Enterprise->IDface)?'':$this->ToImageURL('/enterprise/IDface/'.$Enterprise->IDface);
			 	$Enterprise->IDback=  empty($Enterprise->IDback)?'':$this->ToImageURL('/enterprise/IDback/'.$Enterprise->IDback);
			 }			 
			 return response()->json(['code'=>0,'result' => $Enterprise]);
		}
		public function Save(EnterpriseRepository $EnterpriseRepository,AreaRepository $AreaRepository,Request $request)
		{
			$user = auth('members')->user();
			$Enterprise=$EnterpriseRepository->findBy('MembersID',$user->ID);
			if(empty($Enterprise))
			{
				 return response()->json(['code'=>1,'message' => '您还未创建企业']);
			}
			if($Enterprise->Status==2)
			{
				 return response()->json(['code'=>1,'message' => '审核通过的企业，如果要修改资料，请和管理员联系。']);
			}
			  $Audit=trim($request->input('Audit'));
			  $data=[
						  'ID'=>$this->nextid(),
							 'Name'=>trim($request->input('Name')),
							 'AreaID'=>trim($request->input('AreaID')),
							 'Addrees'=>trim($request->input('Addrees')),
							 'Representative'=>trim($request->input('Representative')),
							 'LicenseNo'=>trim($request->input('LicenseNo')),
							 'LinkmanName'=>trim($request->input('LinkmanName')),
							 'LinkmanPhone'=>trim($request->input('LinkmanPhone')),
							 'Bank'=>trim($request->input('Bank')),
							 'TaxNo'=>trim($request->input('TaxNo')),
							 'Status'=>($Audit=='1')?1:0,
							 'Lng'=>trim($request->input('Lng','0')),
							 'Lat'=>trim($request->input('Lat','0')),
						  ];
			 $Area= $AreaRepository->find(trim($request->input('AreaID')));
			 if(!empty($Area))
			 {
			 	 $data['AreaAllID']=$Area->AllID;
			 }
		   $EnterpriseRepository->update($data,$Enterprise->ID);		
		    
       if($Audit=='1')
       {
       	 
       	  return response()->json(['code'=>0,'message' => '您的企业资料已保存并提交审核！']);		  		
       }
		   return response()->json(['code'=>0,'message' => '您的企业资料保存成功！']);		  
		}
	public function UploadImg($field,Request $request, EnterpriseRepository $EnterpriseRepository)
	{
		 $user = auth('members')->user();
			$Enterprise=$EnterpriseRepository->findBy('MembersID',$user->ID);
			if(empty($Enterprise))
			{
				 return response()->json(['code'=>1,'message' => '您还未创建企业']);
			}
			if($Enterprise->Status==2)
			{
				 return response()->json(['code'=>1,'message' => '审核通过的企业，如果要修改资料，请和管理员联系。']);
			}
			
		 if ($request->hasFile('file')) {

			$file = $request->file('file');
 
			$ext = $file->getClientOriginalExtension(); 
			$realPath = $file->getRealPath();  
		 
			$filename = uniqid() . '.' . $ext;
			if (in_array(strtolower($file->extension()), ['jpeg', 'jpg', 'gif', 'gpeg', 'png'])) {
				 
				$bool = Storage::disk('oss')->put("/enterprise/".$field."/" . $filename, file_get_contents($realPath));
				 
				if (!$bool) {
					return response()->json(['code' => 1, 'message' => '上传失败']);
				}
				else {
					if (trim($Enterprise[$field]) != '') {
						 
						Storage::disk('oss')->delete("/enterprise/".$field."/" . $Enterprise[$field]);
					}
					$Enterprise->$field=$filename;
				  $Enterprise->save();
					return response()->json(['code' => 0, 'message' => '图片上传成功！', 'result' => $filename]);
				}
			}
			else {
				return response()->json(['code' => 1, 'message' => '请选择图片文件']);
			}
		}

	}
		
}
