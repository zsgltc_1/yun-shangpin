<?php

namespace App\Http\Controllers\Members\Wallet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\MembersCashAccountsRepository;
use App\Repositories\Pay_LogRepository;
use App\Repositories\MembersOrdersRepository;
use App\Repositories\SerialNumberRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Log;

//会员钱包
class IndexController extends Controller
{
    public function __construct()
	 {
			$this->middleware('auth:members');
	 }
	 public function Index(Request $request,MembersCashAccountsRepository $MembersCashAccountsRepository)
	 {
	 	 $user = auth('members')->user();
	 	 $pagesize = $request->input('pagesize', '25');
	 	 $result = $MembersCashAccountsRepository->GetListByMembersID($user->ID,$pagesize);
	 	 return response()->json(['code' => 0, 'result' => $result]);
	 }
	 /**
	 *  余额
	 */
	 public function Balance(Request $request,MembersCashAccountsRepository $MembersCashAccountsRepository)
	 {
	 	$user = auth('members')->user();
	  
	   return response()->json(['code' => 0, 'result' => $MembersCashAccountsRepository->Balance($user->ID)]);
	 }
	 public function GetWithdrawConfig()
	 {
	 	
	 	return response()->json(['code' => 0, 'result' => '2%']);
	 }
	  public function Withdraw(Request $request,SerialNumberRepository $SerialNumberRepository,MembersCashAccountsRepository $MembersCashAccountsRepository,MembersOrdersRepository $MembersOrdersRepository)
	  {
	  	$user = auth('members')->user();
	    $Amount=floatval($request->input('Amount'));
	    if($Amount <=0)
	    {
	   	 return response()->json(['code' => 1, 'message' => '提现金额错误！']);
	    }
	    $Balance=$MembersCashAccountsRepository->Balance($user->ID);
	    if($Balance < $Amount)
	    {
	    	return response()->json(['code' => 1, 'message' => '提现金额过多余额不足！']);
	    }
	  	DB::beginTransaction();
	  		try {
	  			$Orders=[
				'ID'=>$this->nextid(),
				'OrderSN'=>$SerialNumberRepository->GetNumber(1,6),
				'MembersID'=>$user->ID,
				'Amount'=>$Amount,
				'Memo'=>'会员提现',
				 'Type'=>2,
				'Status'=>0,
				'CreateTime'=>time(),
				];
				 $MembersCashAccounts=[
		    'Type'=>2,//提现
		   'SourceID'=>$Orders['OrderSN'],
		   'Credit'=>$Amount,
		    'MembersID'=>$user->ID,
		   'Status'=>0,
		   'Memo'=>'会员提现',
		   'CreateTime'=>time(),
		  ];
		    $MembersCashAccountsRepository->create($MembersCashAccounts);
		    $MembersOrdersRepository->create($Orders);
		   
		    
				DB::commit();
				return response()->json(['code' => 0, 'message' => '提现申请成功！请等待业务员与您联系','result'=>$Orders['OrderSN'] ]);
	  		}catch (Exception $e) {
				DB::rollBack();
				Log::error($e->getMessage());
				return ['code' => 1, 'message' => '操作失败！'];
			}
	  }
	 
	 /**
	 * 充值
	 */
	 public function Recharge(Request $request,SerialNumberRepository $SerialNumberRepository,MembersCashAccountsRepository $MembersCashAccountsRepository,MembersOrdersRepository $MembersOrdersRepository,Pay_LogRepository $Pay_LogRepository)
	 {
	 	 $user = auth('members')->user();
	   $Amount=floatval($request->input('Amount'));
	   if($Amount <=0)
	   {
	   	return response()->json(['code' => 1, 'message' => '充值金额错误！']);
	   }
	   DB::beginTransaction();
			try {
				$Orders=[
				'ID'=>$this->nextid(),
				'OrderSN'=>$SerialNumberRepository->GetNumber(1,6),
				'MembersID'=>$user->ID,
				'Amount'=>$Amount,
				'Memo'=>'会员充值',
				 'Type'=>1,
				'Status'=>0,
				'CreateTime'=>time(),
		  ];
		  $Pay_Log=[
		  'ID'=>$this->nextid(),
		  'PaySN'=>$SerialNumberRepository->GetNumber(2,6),
		  'OrderSN'=>$Orders['OrderSN'],
		  'MembersID'=>$user->ID,
		  'Type'=>1,
		  'PaymentMode'=>0,
		  'Status'=>0,
			'CreateTime'=>time(),
			'TotalAmount'=>$Amount,	
		  ];
		  $MembersCashAccounts=[
		  'Type'=>1,//充值
		   'SourceID'=>$Orders['OrderSN'],
		   'Debit'=>$Amount,
		    'MembersID'=>$user->ID,
		   'Status'=>0,
		   'Memo'=>'会员充值',
		   'CreateTime'=>time(),
		  ];
		    $MembersCashAccountsRepository->create($MembersCashAccounts);
		    $MembersOrdersRepository->create($Orders);
		    $Pay_LogRepository->create($Pay_Log);
		    
				DB::commit();
				return response()->json(['code' => 0, 'message' => '下单成功，请跳转到支付页面！','result'=>$Orders['OrderSN']]);
			}catch (Exception $e) {
				DB::rollBack();
				Log::error($e->getMessage());
				return ['code' => 1, 'message' => '操作失败！'];
			}
	 }
	 
}
