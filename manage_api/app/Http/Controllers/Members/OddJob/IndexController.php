<?php

namespace App\Http\Controllers\Members\OddJob;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\OddJobRepository;
use App\Repositories\OddJobDynamicRepository;
use App\Repositories\OddJobDynamicPictureRepository;
use App\Repositories\AreaRepository;
use Storage;
use Log;
//零工主页

class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
		/**
		*  零工主页翻页列表
		*/
		public function Index(Request $request,OddJobRepository $OddJobRepository)
		{
			$user = auth('members')->user();
			 	$pagesize = $request->input('pagesize', '25');
			 	$kw = $request->input('kw', '');
			 	$AreaID=$request->input('AreaID','0');
				$Lng=$request->input('Lng','0');
				$Lat=$request->input('Lat','0');
	 	  $result = $OddJobRepository->GetList($user->ID,$AreaID,$Lat,$Lng,addslashes($kw), [], [], $pagesize, ['Distance' => 'asc']);
	 	  foreach($result as $item)
	 	  {
	 	  	$item->Avatar=$item->Avatar==''?$userInfo->Avatar:$this->ToImageURL('/avatar/users/'.$item->Avatar);
	 	  }
		  return response()->json(['code' => 0, 'result' => $result]);
		}
		public function Save(Request $request,OddJobRepository $OddJobRepository,AreaRepository $AreaRepository)
		{
			$user = auth('members')->user();
			$result=$OddJobRepository->findBy('MembersID',$user->ID);
			$data=[
			'Name'=>trim($request->input('Name')),
			'Gender'=>trim($request->input('Gender')),
			'Wechat'=>trim($request->input('Wechat')),
			'LinkmanPhone'=>trim($request->input('LinkmanPhone')),
			'AreaID'=>trim($request->input('AreaID')),
			'Area'=>trim($request->input('Area')),
			'Skills'=>trim($request->input('Skills')),
			'Introduction'=>trim($request->input('Introduction')),
			'WorkingHours'=>trim($request->input('WorkingHours')),
		  'Email'=>trim($request->input('Email')),
			];
			 $Area= $AreaRepository->find(trim($request->input('AreaID')));
			 if(!empty($Area))
			 {
			 	 $data['AreaAllID']=$Area->AllID;
			 }
			$data['UpdateTime']=time();
			if(empty($result))
			{
				$data['MembersID']=$user->ID;
				$data['CreateTime']=time();
				$OddJobRepository->create($data);
			}else{
				$OddJobRepository->update($data,['ID'=>$result->ID]);
				}
			 return response()->json(['code' => 0, 'message' =>'保存成功！']);
		}
		public function GetModel(Request $request,OddJobRepository $OddJobRepository,OddJobDynamicRepository $OddJobDynamicRepository,OddJobDynamicPictureRepository $OddJobDynamicPictureRepository)
		{
			$user = auth('members')->user();
			$result=$OddJobRepository->GetByMembersID($user->ID);
			
			if(!empty($result))
			{
				$result->Avatar=$result->Avatar==''?'':$this->ToImageURL('/avatar/users/'.$result->Avatar);
				$result->Dynamic=$OddJobDynamicRepository->GetALL(['MembersID'=>$result->MembersID],[],['CreateTime'=>'desc']);
				foreach($result->Dynamic as $Dynamic)
				{
					$Dynamic->Files=$OddJobDynamicPictureRepository->GetALL(['OddJobDynamicID'=>$Dynamic->ID],[],['CreateTime'=>'desc']);
					foreach($Dynamic->Files as $file)
					{

						$file->FileName=$this->ToImageURL('OddJobDynamic/'.$file->FileName);
					}
				}
			}
	    return response()->json(['code' => 0, 'result' =>$result]);
		}
		public function GetByID(Request $request,OddJobRepository $OddJobRepository,OddJobDynamicRepository $OddJobDynamicRepository,OddJobDynamicPictureRepository $OddJobDynamicPictureRepository)
		{
			$ID=$request->input('ID');
			$result=$OddJobRepository->GetByID($ID);
			if(!empty($result))
			{
				$result->Avatar=$result->Avatar==''?'':$this->ToImageURL('/avatar/users/'.$result->Avatar);
				$result->Dynamic=$OddJobDynamicRepository->GetALL(['MembersID'=>$result->MembersID],[],['CreateTime'=>'desc']);
				foreach($result->Dynamic as $Dynamic)
				{
					$Dynamic->Files=$OddJobDynamicPictureRepository->GetALL(['OddJobDynamicID'=>$Dynamic->ID],[],['CreateTime'=>'desc']);
					foreach($Dynamic->Files as $file)
					{
						$file->FileName=$this->ToImageURL('OddJobDynamic/'.$file->FileName);
					}
				}
			}
	    return response()->json(['code' => 0, 'result' =>$result]);
		}
		public function AppendDynamic(Request $request,OddJobRepository $OddJobRepository,OddJobDynamicRepository $OddJobDynamicRepository,OddJobDynamicPictureRepository $OddJobDynamicPictureRepository)
		{
			$user = auth('members')->user();
			$data=[
			'ID'=>$this->nextid(),
			'Content'=>trim($request->input('Content')),
			'Location'=>trim($request->input('Location')),
			'MembersID'=>$user->ID,
			'Status'=>0,
			'CreateTime'=>time(),
			];
			$OddJobDynamicRepository->create($data);
 
				 foreach ($request->file() as $files){
			 
				   if(is_array($files))
				   {
				   		 foreach ($files as $file){
				   		 	$this->SavePicture($file,$OddJobDynamicPictureRepository,$data['ID']);
				   		 	}
				   }else{
				   	$this->SavePicture($files,$OddJobDynamicPictureRepository,$data['ID']);
				   	}
				   
				 }
 
			return response()->json(['code' => 0, 'message' =>'保存成功！']);
		}
	  private function SavePicture($file,$OddJobDynamicPictureRepository,$ID)
	  {
	  	$ext = $file->getClientOriginalExtension();
			$realPath = $file->getRealPath();
			$filename = uniqid() . '.' . $ext;
			if (in_array(strtolower($file->extension()), ['jpeg', 'jpg', 'gif', 'gpeg', 'png'])) {
				$bool = Storage::disk('oss')->put("/OddJobDynamic/" . $filename, file_get_contents($realPath));
				if($bool)
				{
					$data=['OddJobDynamicID'=>$ID,'FileName'=>$filename,'Status'=>0,'CreateTime'=>time()];
					$OddJobDynamicPictureRepository->create($data);
				}
			}
	  }

}
