<?php

namespace App\Http\Controllers\Members\EnterprisePicture;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterprisePictureCategoryRepository as CategoryRepository;
use App\Repositories\EnterprisePictureRepository as PictureRepository;
class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
		//获取企业相册分类信息
		public function Categorys(Request $request,CategoryRepository $CategoryRepository)
		{
			$ID=$request->input('ID');
			$result=$CategoryRepository->GetALL(['EnterpriseID'=>$ID],[],['Sort'=>'ASC']);
			foreach($result as $item)
			{
				$item->Cover=empty($item->Cover)?'':$this->ToImageURL('/EnterprisePicture/'.$item->Cover);
			}
			return response()->json(['code' => 0, 'message' => 'OK！', 'result' => $result]);
		}
	  public function Pictures(Request $request,PictureRepository $PictureRepository,CategoryRepository $CategoryRepository)
		{
			$ID=$request->input('ID');
			$Category=$CategoryRepository->find($ID);
			if(!empty($Category))
				{
				$Category->Cover=empty($Category->Cover)?'':$this->ToImageURL('/EnterprisePicture/'.$Category->Cover);
				$Category->list=$PictureRepository->GetALL(['EnterprisePictureCategoryID'=>$ID],[],['Sort'=>'ASC']);
				foreach($result as $item)
				{
					$item->FileName=empty($item->FileName)?'':$this->ToImageURL('/EnterprisePicture/'.$item->FileName);
				}
				
				return response()->json(['code' => 0, 'message' => 'OK！', 'result' =>$Category ]);
			}
			return response()->json(['code' => 1, 'message' => '没有数据！']);
		}
}
