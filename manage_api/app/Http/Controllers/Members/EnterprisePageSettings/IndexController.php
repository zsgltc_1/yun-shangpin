<?php

namespace App\Http\Controllers\Members\EnterprisePageSettings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Repositories\EnterprisePageSettingsRepository as PageSettingsRepository;

class IndexController extends Controller
{
  public function __construct()
	{
		$this->middleware('auth:members');

	}
	public function GetModel(PageSettingsRepository $PageSettingsRepository)
	{
		$model=$PageSettingsRepository->findBy('Status',0);
		return response()->json(['code' => 0, 'result' => $model]);
	}
}
