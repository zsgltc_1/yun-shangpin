<?php

namespace App\Http\Controllers\Members\EnterprisePosition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterprisePositionRepository;
use App\Repositories\EnterpriseRepository;
class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
		public function ChoicenessList(Request $request,EnterprisePositionRepository $EnterprisePositionRepository)
		{
			$user = auth('members')->user();
			$AreaID=$request->input('AreaID','0');
			$Lng=$request->input('Lng','0');
			$Lat=$request->input('Lat','0');
		  
		  return response()->json(['code' => 0, 'result' => $EnterprisePositionRepository->ChoicenessList($user->ID,$AreaID,$Lat,$Lng)]);
			
		}
	 public function GetList(Request $request, EnterprisePositionRepository $EnterprisePositionRepository)
	 {
	 	$user = auth('members')->user();
	 	$pagesize = $request->input('pagesize', '25');
	 	$kw = $request->input('kw', '');
	 	$AreaID=$request->input('AreaID','0');
		$Lng=$request->input('Lng','0');
		$Lat=$request->input('Lat','0');
	 	$result = $EnterprisePositionRepository->GetList($user->ID,$AreaID,$Lat,$Lng,addslashes($kw), [], [], $pagesize, ['Distance' => 'asc']);
	 	return response()->json(['code' => 0, 'result' => $result]);
	 }
	 public function Infos(Request $request, EnterprisePositionRepository $EnterprisePositionRepository,EnterpriseRepository $EnterpriseRepository)
	 {
	 	  $user = auth('members')->user();
	 	  $Lng=$request->input('Lng','0');
		  $Lat=$request->input('Lat','0');
		  $ID=$request->input('ID','0');
		  $infos=$EnterprisePositionRepository->Infos($user->ID,$ID,$Lat,$Lng);
		  if(empty($infos))
		  {
		  	 return response()->json(['code' => 1,'message'=>'找不到数据']);
		  }
		  $infos->Enterprise=$EnterpriseRepository->find($infos->EnterpriseID);
		  return response()->json(['code' => 0, 'result' => $infos]);
	 }
}
