<?php

namespace App\Http\Controllers\Members\JobCategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
}
