<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseRepository;
use App\Repositories\MembersInEnterpriseRepository;

abstract class EnterpriseController extends Controller
{
    protected $user=null;
	  protected $EnterpriseID=null;
	  protected $Enterprise=null;
    public function __construct(EnterpriseRepository $EnterpriseRepository,MembersInEnterpriseRepository $MembersInEnterpriseRepository)
		{
			 $this->middleware('auth:members');
			 $this->user = auth('members')->user();
			 $this->Enterprise=$EnterpriseRepository->findBy('MembersID',$this->user->ID);
			 if(empty($this->Enterprise))
			 {
			 	 $MembersInEnterprise=$MembersInEnterpriseRepository->findBy('MembersID',$this->user->ID);
			 	 if(!empty($MembersInEnterprise))
			 	 {
			 	 	$this->Enterprise=$EnterpriseRepository->find($MembersInEnterprise->EnterpriseID);
			 	 }
			 }
		}
}
