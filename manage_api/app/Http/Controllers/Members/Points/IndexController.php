<?php

namespace App\Http\Controllers\Members\Points;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Repositories\MembersPointsAccountsRepository;


/**
* 会员积分相关
*/
class IndexController extends Controller
{
   public function __construct()
	 {
			$this->middleware('auth:members');
	 }
		/**
		* 积分明细
		*/
	 public function Index(Request $request,MembersPointsAccountsRepository $MembersPointsAccountsRepository)
	 {
	 	 $user = auth('members')->user();
	 	 $pagesize = $request->input('pagesize', '25');
	 	 $result = $MembersPointsAccountsRepository->GetListByMembersID($user->ID,$pagesize);
	 	 return response()->json(['code' => 0, 'result' => $result]);
	 }
	 /**
	 *  收支统计
	 */
	 public function IncomeExpenses(Request $request,MembersPointsAccountsRepository $MembersPointsAccountsRepository)
	 {
	 	$user = auth('members')->user();
	 	$Debit=$MembersPointsAccountsRepository->GetSumDebit($user->ID);
	 	$Credit=$MembersPointsAccountsRepository->GetSumCredit($user->ID);
	 	return response()->json(['code' => 0, 'result' => 
	 	[
	 	'Debit'=>$Debit,
	 	'Credit'=>$Credit, 
	 	 'Balance'=>$Debit - $Credit,
	 	]
	 	]);
	 }
	  /**
	 *  每天奖励
	 */
 	 public function DailyReward(Request $request,MembersPointsAccountsRepository $MembersPointsAccountsRepository)
 	 {
 	 	
 	 	$user = auth('members')->user();
 	 	return response()->json(['code' => 0, 'result' => $MembersPointsAccountsRepository->DailyReward($user->ID,20)]);
 	 }
 	 public function SignInInfos(Request $request,MembersPointsAccountsRepository $MembersPointsAccountsRepository)
 	 {
 	 	  $user = auth('members')->user();
 	 	 	return response()->json(['code' => 0, 'result' =>$MembersPointsAccountsRepository->SignInInfos($user->ID)]);
 	 }
 	 public function SignIned(Request $request,MembersPointsAccountsRepository $MembersPointsAccountsRepository)
 	 {
 	 	$user = auth('members')->user();
 	 	return response()->json(['code' => 0, 'result' =>$MembersPointsAccountsRepository->SignIned($user->ID)]);
 	 }
 	 
}
