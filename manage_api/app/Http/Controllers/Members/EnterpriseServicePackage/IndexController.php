<?php

namespace App\Http\Controllers\Members\EnterpriseServicePackage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseServicePackageRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Log;
/**
* 企业服务套餐
*/
class IndexController extends Controller
{
    public function __construct()
		{
			$this->middleware('auth:members');
		}
	  public function GetList(Request $request,EnterpriseServicePackageRepository $EnterpriseServicePackageRepository)
		{
			 $result=$EnterpriseServicePackageRepository->GetALL(['Status'=>0],[],['Price'=>'ASC']);
			 return response()->json(['code' => 0, 'message' => 'OK！', 'result' => $result]);
		}
		
}
