<?php

namespace App\Http\Controllers\Members\History;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\MembersHistoryRepository;

/**
* 会员足迹
*/
class IndexController extends Controller
{
    public function __construct()
	 {
			$this->middleware('auth:members');
	 }
	 public function Index(Request $request,MembersHistoryRepository $MembersHistoryRepository)
	 {
	 	 $user = auth('members')->user();
	 	 $pagesize = $request->input('pagesize', '25');
	 	 $result = $MembersHistoryRepository->GetListByMembersID($user->ID,$pagesize);
	 	 return response()->json(['code' => 0, 'result' => $result]);
	 }
	 public function Delete(Request $request,MembersHistoryRepository $MembersHistoryRepository)
	 {
	 	 $ID= $request->input('ID');
	 	  $user = auth('members')->user();
	 	 $model=$MembersHistoryRepository->find($ID);
	 	 if(!empty($model) &&  $model->MembersID==$user->ID)
	 	 {
	 	 	$result =  $model->delete();
	 	 	return response()->json(['code' => 0, 'result' => $result,'message'=>'操作成功']);
	 	 }
	 	 else{
	 	 	return response()->json(['code' => 1, 'message'=>'操作失败']);
	 	 	}
	 }
	 public function Append(Request $request,MembersHistoryRepository $MembersHistoryRepository)
	 {
	 	  $user = auth('members')->user();
	 	  $data=[
	 	  'MembersID'=>$user->ID,
	 	  'Title'=>trim($request->input('Title')),
	 	  'URL'=>trim($request->input('URL')),
	 	  'CreateTime'=>time(),
	 	  'Status'=>0,
	 	  ];
	 	   	$result =  $MembersHistoryRepository->create($data);
	 	 return response()->json(['code' => 0, 'result' => $result,'message'=>'操作成功']);
	 }
}
