<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CaptchasController extends Controller
{
    public function code()
    {
        return [
            'code' => 0,
            'message' => '获取验证码成功',
            'result' => app('captcha')->create('default', true) //create是生成验证码的方法
        ];
    }
}
