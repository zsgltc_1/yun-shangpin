<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SiteNavigationsRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
 
/**
 * 
 */
class IndexController extends Controller
{

	public function __construct()
	{
	 
	}
	public function GetMenus(SiteNavigationsRepository $SiteNavigationsRepository, Request $request)
	{
	 
	 
		$this->list=$SiteNavigationsRepository->GetMenu();
		$data = $this->GetMenuList(0);
		 
		return response()->json(['code' => 0, 'data' => $data, 'type' => 'adminMenu']);
	}
	protected function GetMenuList($ParentID)
	{
		$list = [];
		foreach ($this->list as $item) {
			if ($item->ParentID == $ParentID) {
				$list[] = [
					"path" => $item->Path,
					"name" => $item->Path,
					"component" => $item->Component,
					"redirect" => $item->Redirect,
					"props" => true,
					"meta" => [
						"title" =>  $item->Text,
						"isLink" => $item->IsLink == 1 ? true : "",
						"isKeepAlive" => false,
						"isAffix" => false,
						"isIframe" => false,
						"roles" => ["users"],
						"icon" => $item->Ico,
						"isHide" => $item->IsHide == 1 ? true : ""
					],
					'children' => $this->GetMenuList($item->ID)
				];
			}
		}
		return $list;
	}
	 
}