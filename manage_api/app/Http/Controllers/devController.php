<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Log;
use Artisan;
class devController extends Controller
{


	public function index()
	{

		date_default_timezone_set("Asia/Shanghai");
		$database = config('database.connections.' . config('database.default') . '.database');
		$table_list = DB::select('SHOW TABLE STATUS FROM ' . $database);
		$prefix = config('database.connections.' . config('database.default') . '.prefix');
		foreach ($table_list as &$table) {

			$table->Name = str_replace($prefix, '', $table->Name);

		}
		$this->output['table_list'] = $table_list;
		return View::make('dev', $this->output);
	}
	public function createrepository(Request $request)
	{

		date_default_timezone_set("Asia/Shanghai");
		$database = config('database.connections.' . config('database.default') . '.database');
		$prefix = config('database.connections.' . config('database.default') . '.prefix');
		$table_name = $request->input('table_name', []);
		foreach ($table_name as $table) {
			$table_info = DB::select('SHOW TABLE STATUS  WHERE Name=\'' . $prefix . $table . '\'')[0];

			$sql_tab = 'show full fields from `' . $prefix . $table . '`';
			$fields = DB::select($sql_tab);
			$keys = DB::select('show keys from `' . $prefix . $table . '` Where Key_name=\'PRIMARY\'');
			$keyType_name = 'int';
			$incrementing = 'false';
			$arr = [];
			$castslist = [];

			if (count($keys) > 0) {
				$primaryKey = $keys[0];
				foreach ($fields as $f) {
					$keyType_name = preg_replace('/\(\d+\)/', '', $f->Type);
					if ($keyType_name == 'bigint' && $f->Field != 'CreateTime') {
						$castslist[] = "'" . $f->Field . "'=>'string'";
					}
					if ($f->Field != $primaryKey->Column_name) {
						$arr[] = "'" . $f->Field . "'";
					}
					else {
						$keyType_name = preg_replace('/\(\d+\)/', '', $f->Type);
						switch ($keyType_name) {
							case 'int':
								$keyType_name = 'int';
								break;
							case 'bigint':
								$keyType_name = 'string';

								break;
							default:
								$keyType_name = 'string';
								break;
						}
						if ($f->Extra != 'auto_increment') {
							$arr[] = "'" . $f->Field . "'";
						}
						else {
							$incrementing = 'true';
						}
					}

				}
			}
			else {
				foreach ($fields as $f) {
					$keyType_name = preg_replace('/\(\d+\)/', '', $f->Type);
					if ($keyType_name == 'bigint' && $f->Field != 'CreateTime') {
						$castslist[] = "'" . $f->Field . "'=>'string'";
					}
					$arr[] = "'" . $f->Field . "'";
				}
			}

			$exitCode = Artisan::call('make:repository', [
				'repository' => $table, '--model' => ucfirst($table)
			]);

			$exitCode = Artisan::call('make:modelclass', [
				'model' => ucfirst($table),
				'--table_name' => $table,
				'--primaryKey_name' => $primaryKey->Column_name,
				'--keyType_name' => $keyType_name,
				'--isincrementing' => $incrementing,
				'--castslist' => join(",
										", $castslist),
				'--fileds' => join(",
    									 ", $arr),
			]);

		}

		return '操作完成';
	}
	public function createrepository2(Request $request)
	{

		date_default_timezone_set("Asia/Shanghai");
		$database = config('database.connections.' . config('database.default') . '.database');
		$prefix = config('database.connections.' . config('database.default') . '.prefix');
		$table_name = $request->input('table_name', []);
		foreach ($table_name as $table) {
			$table_info = DB::select('SHOW TABLE STATUS  WHERE Name=\'' . $prefix . $table . '\'')[0];

			$sql_tab = 'show full fields from `' . $prefix . $table . '`';
			$fields = DB::select($sql_tab);
			$keys = DB::select('show keys from `' . $prefix . $table . '` Where Key_name=\'PRIMARY\'');
			$arr = [];
			$keyType_name = 'int';
			$incrementing = 'false';
			$castslist = [];
			if (count($keys) > 0) {
				$primaryKey = $keys[0];

				foreach ($fields as $f) {
					$keyType_name = preg_replace('/\(\d+\)/', '', $f->Type);
					if ($keyType_name == 'bigint' && $f->Field != 'CreateTime') {
						$castslist[] = "'" . $f->Field . "'=>'string'";
					}
					if ($f->Field != $primaryKey->Column_name) {
						$arr[] = "'" . $f->Field . "'";
					}
					else {
						$keyType_name = preg_replace('/\(\d+\)/', '', $f->Type);
						switch ($keyType_name) {
							case 'int':
								$keyType_name = 'int';
								break;
							case 'bigint':
								$keyType_name = 'string';
								break;
							default:
								$keyType_name = 'string';
								break;
						}
						if ($f->Extra != 'auto_increment') {
							$arr[] = "'" . $f->Field . "'";
						}
						else {
							$incrementing = 'true';
						}

					}

				}
			}
			else {
				foreach ($fields as $f) {
					$keyType_name = preg_replace('/\(\d+\)/', '', $f->Type);
					if ($keyType_name == 'bigint' && $f->Field != 'CreateTime') {
						$castslist[] = "'" . $f->Field . "'=>'string'";
					}
					$arr[] = "'" . $f->Field . "'";
				}
			}
			$exitCode = Artisan::call('make:modelclass', [
				'model' => ucfirst($table),
				'--table_name' => $table,
				'--primaryKey_name' => $primaryKey->Column_name,
				'--keyType_name' => $keyType_name,
				'--isincrementing' => $incrementing,
				'--castslist' => join(",
										", $castslist),
				'--fileds' => join(",
    									 ", $arr),
			]);
		}

		return '操作完成';
	}
	public function createForm(Request $request,$table_name)
	{
		
		if(!empty($table_name))
		{
			date_default_timezone_set("Asia/Shanghai");
		  $database = config('database.connections.' . config('database.default') . '.database');
		  $prefix = config('database.connections.' . config('database.default') . '.prefix');
			$sql_tab = 'show full fields from `' . $prefix . $table_name . '`';
			$fields = DB::select($sql_tab);
			$this->output['fields'] = $fields;
		  $this->output['table_name'] = $table_name;
		  return View::make('createform', $this->output);
		}
	}
}