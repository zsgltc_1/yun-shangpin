<?php
namespace App\Http\Controllers\Admin\EnterpriseOrders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseOrdersRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
/**
 *  企业审核管理
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');

	}
	 public function GetList(Request $request, EnterpriseOrdersRepository $EnterpriseOrdersRepository)
	 {
	 	$pagesize = $request->input('pagesize', '25');
	 	$kw = $request->input('kw', '');
	 	$result = $EnterpriseOrdersRepository->GetList(addslashes($kw), [], [], $pagesize, ['EnterpriseOrders.CreateTime' => 'desc']);
	 	return response()->json(['code' => 0, 'result' => $result]);
	 }

}
