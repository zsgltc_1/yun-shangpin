<?php
namespace App\Http\Controllers\Admin\EnterpriseServicePackage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseServicePackageRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Repositories\UsersInRolesRepository;
/**
 *  企业服务套餐
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');

	}
	 public function GetList(Request $request, EnterpriseServicePackageRepository $EnterpriseServicePackageRepository)
	 {
	 	$pagesize = $request->input('pagesize', '25');
		$kw = $request->input('kw', '');
		$result = $EnterpriseServicePackageRepository->GetList(addslashes($kw), [], [], $pagesize, ['CreateTime' => 'desc']);
		return response()->json(['code' => 0, 'result' => $result]);
	 }
	 public function Create(EnterpriseServicePackageRepository $EnterpriseServicePackageRepository, Request $request)
	 {
	    $data=[ 'ID' => $this->nextid(),
					 'Name'=>trim($request->input('Name')),
					 'OriginalPrice'=>trim($request->input('OriginalPrice')),
					 'Price'=>trim($request->input('Price')),
					 'Months'=>trim($request->input('Months')),
					 'Description'=>trim($request->input('Description')),
					 'CreateTime' => time(),
					 ];
		 if( $EnterpriseServicePackageRepository->Create($data))
			{
					return response()->json(['code' => 0, 'message' => '创建成功！']);
			}else{
					return response()->json(['code' => 1, 'message' => '创建失败！']);
			}
	 }
   public function Update(EnterpriseServicePackageRepository $EnterpriseServicePackageRepository, Request $request)
	 {
	 	 $ID = trim($request->input('ID'));
	    $data=[ 
					 'Name'=>trim($request->input('Name')),
					 'OriginalPrice'=>trim($request->input('OriginalPrice')),
					 'Price'=>trim($request->input('Price')),
					 'Months'=>trim($request->input('Months')),
					 'Description'=>trim($request->input('Description')),
					 ];
					 $EnterpriseServicePackageRepository->update($data,$ID);
					 	return response()->json(['code' => 0, 'message' => '修改成功！']);
	 
	 }
	public function Enable(EnterpriseServicePackageRepository $EnterpriseServicePackageRepository, Request $request)
	{
		$ID = $request->input('ID');
		$model = $EnterpriseServicePackageRepository->find($ID);
		if ($model->update(['Status' => 0])) {
			return response()->json(['code' => 0, 'message' => '启用成功']);
		}
		else {
			return response()->json(['code' => 1, 'message' => '启用失败']);
		}
	}
	public function Disable(EnterpriseServicePackageRepository $EnterpriseServicePackageRepository, Request $request)
	{
		$ID = $request->input('ID');
		$model = $EnterpriseServicePackageRepository->find($ID);

		if ($model->update(['Status' => 1])) {
			return response()->json(['code' => 0, 'message' => '禁用成功']);
		}
		else {
			return response()->json(['code' => 1, 'message' => '禁用失败']);
		}
	}
	public function Delete(EnterpriseServicePackageRepository $EnterpriseServicePackageRepository, Request $request)
	{
		$ID = $request->input('ID');
		$model = $EnterpriseServicePackageRepository->find($ID);

		if (!empty($model)) {
			$model->delete();
			return response()->json(['code' => 0, 'message' => '删除操作成功！']);
		}
		else {
			return response()->json(['code' => 1, 'message' => '删除失败！']);
		}
	}
}
