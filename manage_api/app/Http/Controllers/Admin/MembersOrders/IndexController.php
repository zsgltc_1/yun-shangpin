<?php
namespace App\Http\Controllers\Admin\MembersOrders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\MembersOrdersRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
/**
 *  企业审核管理
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');

	}
	 public function GetList(Request $request, MembersOrdersRepository $MembersOrdersRepository)
	 {
	 	$pagesize = $request->input('pagesize', '25');
	 	$kw = $request->input('kw', '');
	 	$result = $MembersOrdersRepository->GetList(addslashes($kw), [], [], $pagesize, ['MembersOrders.CreateTime' => 'desc']);
	 	return response()->json(['code' => 0, 'result' => $result]);
	 }

}
