<?php
namespace App\Http\Controllers\Admin\EnterpriseUsers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseUsersRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Repositories\UsersInRolesRepository;
/**
 *  企业审核管理
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');

	}
     public function GetList(Request $request, EnterpriseUsersRepository $EnterpriseUsersRepository)
     {
     	$pagesize = $request->input('pagesize', '25');
     		$kw = $request->input('kw', '');
     		$result = $EnterpriseUsersRepository->GetList(addslashes($kw), [], [], $pagesize, ['EnterpriseUsers.CreateTime' => 'desc']);
     		return response()->json(['code' => 0, 'result' => $result]);
     }
     /**
      *  创建账号
      */
     public function Create(EnterpriseUsersRepository $EnterpriseUsersRepository, Request $request)
     {

     	$EnterpriseUsers = [
     		'ID' => $this->nextid(),
     		'EnterpriseID' => trim($request->input('EnterpriseID')),
     		'AccountName' => trim($request->input('AccountName')),
     		'Password' => bcrypt(trim($request->input('Password'))),
     		'MobileTelephone' => trim($request->input('MobileTelephone')),
     		'Email' => trim($request->input('Email')),
     		'QQ' => trim($request->input('QQ')),
     		'Wechat' => trim($request->input('Wechat')),
     		'CreateTime' => time(),
     	];
        // return $EnterpriseUsers;
     	if (!$EnterpriseUsersRepository->existsWhere(['AccountName' => $EnterpriseUsers['AccountName']])) {
     		if ($EnterpriseUsersRepository->create($EnterpriseUsers)) {
     			return response()->json(['code' => 0, 'message' => '创建企业账号成功！']);
     		}
     		else {
     			return response()->json(['code' => 1, 'message' => '抱歉，创建企业账号失败！']);
     		}
     	}
     	else {
     		return response()->json(['code' => 1, 'message' => '账号已被占用']);
     	}
     }
     /* 检查账号是否存在 */
     public function Exist(EnterpriseUsersRepository $EnterpriseUsersRepository, Request $request)
     {
     	$AccountName = trim($request->input('AccountName'));
     	$ID = trim($request->input('ID'));
     	return response()->json(['code' => 0, 'result' => $EnterpriseUsersRepository->existsWhere([['AccountName', '=', $AccountName], ['ID', '<>', $ID]])]);
     }
     /* 启用账号 */
     public function Enable(EnterpriseUsersRepository $EnterpriseUsersRepository, Request $request)
     {
     	$ID = $request->input('ID');
     	$model = $EnterpriseUsersRepository->find($ID);
     	if ($model->update(['Status' => 0])) {
     		return response()->json(['code' => 0, 'message' => '账号启用成功']);
     	}
     	else {
     		return response()->json(['code' => 1, 'message' => '账号启用失败']);
     	}
     }
     /* 禁用账号 */
     public function Disable(EnterpriseUsersRepository $EnterpriseUsersRepository, Request $request)
     {
     	$ID = $request->input('ID');
     	$model = $EnterpriseUsersRepository->find($ID);

     	if ($model->update(['Status' => 1])) {
     		return response()->json(['code' => 0, 'message' => '账号禁用成功']);
     	}
     	else {
     		return response()->json(['code' => 1, 'message' => '账号禁用失败']);
     	}
     }
}