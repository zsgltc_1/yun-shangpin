<?php
namespace App\Http\Controllers\Admin\Enterprise;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Repositories\AreaRepository;
/**
 *  企业审核管理
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');
		 
	}
	 public function GetList(Request $request, EnterpriseRepository $EnterpriseRepository,AreaRepository $AreaRepository)
	 {
	 	$pagesize = $request->input('pagesize', '25');
		$kw = $request->input('kw', '');
		$status=$request->input('status', '');
		$where=empty($status)?[]:['Status'=>$status];
		$result = $EnterpriseRepository->GetList(addslashes($kw), $where, [], $pagesize, ['CreateTime' => 'desc']);
		foreach($result as $Enterprise){
			
			  $Enterprise->LicensePicture=  empty($Enterprise->LicensePicture)?'':$this->ToImageURL('/enterprise/LicensePicture/'.$Enterprise->LicensePicture);
			 	$Enterprise->IDface=  empty($Enterprise->IDface)?'':$this->ToImageURL('/enterprise/IDface/'.$Enterprise->IDface);
			 	$Enterprise->IDback=  empty($Enterprise->IDback)?'':$this->ToImageURL('/enterprise/IDback/'.$Enterprise->IDback);
			 	$Enterprise->FullArea=$AreaRepository->GetFullArea($Enterprise->AreaID);
		}	 	
		return response()->json(['code' => 0, 'result' => $result]);
	 }
	 public function GetModel(EnterpriseRepository $EnterpriseRepository, Request $request,AreaRepository $AreaRepository)
	{
		$result = $EnterpriseRepository->find($request->input('ID', '0'));
    if(!empty($result))
    {
    	$result->FullArea=$AreaRepository->GetFullArea($result->AreaID);
    	$result->LicensePicture=  empty($result->LicensePicture)?'':$this->ToImageURL('/enterprise/LicensePicture/'.$result->LicensePicture);
			$result->IDface=  empty($result->IDface)?'':$this->ToImageURL('/enterprise/IDface/'.$result->IDface);
			$result->IDback=  empty($result->IDback)?'':$this->ToImageURL('/enterprise/IDback/'.$result->IDback);
    }
		return response()->json(['code' => 0, 'result' => $result]);
	}
		 
}