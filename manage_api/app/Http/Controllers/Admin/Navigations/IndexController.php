<?php
namespace App\Http\Controllers\Admin\Navigations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SiteNavigationsRepository as menusRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use Exception;
/**
 *  导航管理
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');
		//$this->middleware('UsersPermissions');
	}
	public function Tree(menusRepository $menusRepository)
	{
		$list = $menusRepository->AllByOrder('Orders', 'asc', ['ID', 'Text', 'ParentID', 'ID', 'LangKey']);
		return response()->json(['code' => 0, 'result' => $list]);
	}
	public function GetModel(menusRepository $menusRepository, Request $request)
	{
		$result = $menusRepository->find($request->input('ID', '0'));

		return response()->json(['code' => 0, 'result' => $result]);
	}
	public function Save(menusRepository $menusRepository, Request $request)
	{
		$requestData = [
			'ParentID' => trim($request->input('ParentID')),
			'Text' => trim($request->input('Text')),
			'Ico' => trim($request->input('Ico')),
			'Path' => trim($request->input('Path')),
			'LangKey' => trim($request->input('LangKey')),
			'MobileIco' => trim($request->input('MobileIco')),
			'Component' => trim($request->input('Component')),
			'Readme' => trim($request->input('Readme')),
			'Help' => trim($request->input('Help')),
			'IsLink' => trim($request->input('IsLink')) == '1' ? 1 : 0,
			'IsHide' => trim($request->input('IsHide')) == '1' ? 1 : 0,
			'Redirect' => trim($request->input('Redirect')),
		];
		$ID = $request->input('ID');

		if ($ID > 0) {

			$model = $menusRepository->find($ID);
			$model->update($requestData);

			return response()->json(['code' => 0, 'message' => '修改成功']);
		}
		else {
			$requestData['ID'] = $this->nextid();
			$requestData['CreateTime'] = time();
			$menusRepository->create($requestData);
			$menusRepository->UpdateAll();
			return response()->json(['code' => 0, 'message' => '保存成功']);
		}

	}
	public function DelItem(menusRepository $menusRepository, Request $request)
	{

		$ID = $request->input('ID');
		$result = $menusRepository->find($ID);
		$exists = $menusRepository->existsWhere([['ParentID', '=', trim($request->input('ID', '0'))]]);
		if ($exists) {
			return response()->json(['message' => '因为包含下属菜单，所以删除失败', 'code' => 1]);
		}
		else {
			$result->delete();
			$menusRepository->UpdateAll();
			return response()->json(['message' => '删除成功', 'code' => 0]);
		}
	}
	public function MoveTo(menusRepository $menusRepository, Request $request)
	{

		$ID = trim($request->input('ID', '0'));
		$ParentID = trim($request->input('ParentID', '0'));
		$index = intval($request->input('Index'));
		$menu = $menusRepository->find($ID);
		$menu->ParentID = $ParentID;
		$menu->Sort = $index;
		$menu->save();
		$list = $menusRepository->GetListByParentID($ParentID);

		if (!empty($list)) {
			$i = 0;
			foreach ($list as $val) {
				if ($i == $index) {
					$i++;
				}
				$val->Sort = $i;
				if ($val->ID != $menu->ID) {
					$val->save();
				}
				$i++;
			}
		}

		$menusRepository->UpdateAll();
		return response()->json(['message' => '节点移动成功', 'code' => 0]);
	}
	public function LangKeyExist(menusRepository $menusRepository, Request $request)
	{
		$LangKey = trim($request->input('LangKey'));
		$ID = trim($request->input('ID'));
		return response()->json(['code' => 0, 'result' => $menusRepository->existsWhere([['LangKey', '=', $LangKey], ['ID', '<>', $ID]])]);
	}
	public function GetURI(URIAndMenuRepository $URIAndMenuRepository, Request $request)
	{
		$MenuID = trim($request->input('MenuID'));
		return response()->json(['code' => 0, 'result' => $URIAndMenuRepository->findWhere([['MenuID', '=', $MenuID]])]);
	}
	public function SaveURI(URIAndMenuRepository $URIAndMenuRepository, Request $request)
	{
		$ID = $request->input('ID');
		$requestData = [
			'MenuID' => trim($request->input('MenuID')),
			'Name' => trim($request->input('Name')),
			'URI' => trim($request->input('URI')),
		];
		if ($ID > 0) {

			$model = $URIAndMenuRepository->find($ID);
			$model->update($requestData);

			return response()->json(['code' => 0, 'message' => '修改成功']);
		}
		else {
			$URIAndMenuRepository->create($requestData);
			return response()->json(['code' => 0, 'message' => '保存成功']);
		}
	}
	public function DelteteURI(URIAndMenuRepository $URIAndMenuRepository, Request $request)
	{
		$ID = $request->input('ID');
		$URIAndMenuRepository->delete($ID);
		return response()->json(['code' => 0, 'message' => '完成']);
	}
}
