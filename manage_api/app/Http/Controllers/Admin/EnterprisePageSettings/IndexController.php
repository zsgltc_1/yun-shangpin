<?php

namespace App\Http\Controllers\Admin\EnterprisePageSettings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Repositories\EnterprisePageSettingsRepository as PageSettingsRepository;
class IndexController extends Controller
{
  public function __construct()
	{
		$this->middleware('auth:users');

	}
	public function GetModel(PageSettingsRepository $PageSettingsRepository)
	{
		$model=$PageSettingsRepository->findBy('Status',0);
		if(empty($model))
		{
			$PageSettingsRepository->create(['ID'=>$this->nextid(),'Status'=>0,'CreateTime'=>time(),'Content'=>'']);
			$model=$PageSettingsRepository->findBy('Status',0);
		}
		return response()->json(['code' => 0, 'result' => $model]);
	}
	public function SaveModel(PageSettingsRepository $PageSettingsRepository,Request $request)
	{
		$model=$PageSettingsRepository->findBy('Status',0);
		if(empty($model))
		{
				$PageSettingsRepository->create(['ID'=>$this->nextid(),'Status'=>0,'CreateTime'>time(),'Content'=>'']);
				$model=$PageSettingsRepository->findBy('Status',0);
		}
		$Content=trim($request->input('Content'));
		$model->Content=$Content;
		$model->save();
		return response()->json(['code' => 0, 'result' => $model,'message'=>'保存成功！']);
	}
}
