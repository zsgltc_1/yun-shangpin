<?php
namespace App\Http\Controllers\Admin\Area;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AreaRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Repositories\UsersInRolesRepository;
/**
 *  地域
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');
		 
	}
  public function GetFullArea($areaid ,AreaRepository $AreaRepository)
  {
  	return response()->json(['code' => 0, 'result' => $AreaRepository->GetFullArea($areaid)]);
  }
}