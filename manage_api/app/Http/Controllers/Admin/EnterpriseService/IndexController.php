<?php
namespace App\Http\Controllers\Admin\EnterpriseService;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EnterpriseServiceRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Repositories\UsersInRolesRepository;
/**
 *  企业审核管理
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');

	}
	 public function GetList(Request $request, EnterpriseServiceRepository $EnterpriseServiceRepository)
	 {
	 	$pagesize = $request->input('pagesize', '25');
		$kw = $request->input('kw', '');
		$result = $EnterpriseServiceRepository->GetList(addslashes($kw), [], [], $pagesize, ['CreateTime' => 'desc']);
		return response()->json(['code' => 0, 'result' => $result]);
	 }

}
