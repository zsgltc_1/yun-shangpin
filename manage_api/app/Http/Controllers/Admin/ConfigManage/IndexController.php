<?php
namespace App\Http\Controllers\Admin\ConfigManage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SystemConfigRepository as menusRepository;
use Log;
use Storage;
use Illuminate\Support\Facades\DB;
use Exception;
/**
 * 后台管理后台用户菜单
 */
class IndexController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:users');

	}
    /* 获取配置菜单 */
	public function ParentList(menusRepository $menusRepository)
	{
		$list = $menusRepository->GetListByParentID();
		return response()->json(['code' => 0, 'result' => $list]);
	}

    /* 获取配置菜单数据 */
	public function GetList(menusRepository $menusRepository, Request $request)
	{
        $id = $request->input('id');
        $result = $menusRepository->GetListByParentID($id);
		return response()->json(['code' => 0, 'result' => $result]);
	}

    /* 添加配置菜单 */
	public function Save(menusRepository $menusRepository, Request $request)
	{
        $Type = trim($request->input('Type'));
        $Multiple = trim($request->input('Multiple'));
        if($Type=='select'){
            if($Multiple==1) {
                $requestData = [
                	'Value' => $request->input('Value'),
                ];
            }else {
                $requestData = [
                	'Value' => trim($request->input('Value')),
                ];
            }
        }else if($Type=='checkbox') {
            $requestData = [
            	'Value' => $request->input('Value'),
            ];
        } else {
            $requestData = [
            	'Value' => trim($request->input('Value')),
            ];
        }
		$ID = $request->input('ID');
        $model = $menusRepository->find($ID);
        $model->update($requestData);
        return response()->json(['code' => 0, 'message' => '修改成功']);
	}
}
