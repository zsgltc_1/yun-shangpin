<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use GuzzleHttp\Client;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function nextid()
    {
    	  if (substr(php_uname(), 0, 7) == "Windows"){ 
              $http = new Client();
            $response = $http->get("https://api.pydz.net");
            return $response->getBody()->getContents();
			    } 
			    else { 
			        $snowflake = app('Kra8\Snowflake\Snowflake');
            return $snowflake->next();  
			  } 
        
    }
  
    public function ToImageURL($path){
    	
    	return env('ALIOSS_DOMAIN', null).$path;
    }
}
